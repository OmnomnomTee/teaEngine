//
// Created by tim on 16.08.20.
//

#ifndef TEAENGINECOMPLETE_IMGUILAYER_H
#define TEAENGINECOMPLETE_IMGUILAYER_H

#include <TeaEngine/Core/Layer.h>

namespace teaEngine {

    class ImGuiLayer : public Layer{
    public:
        ImGuiLayer();
        ~ImGuiLayer() override = default;

        void onAttach() override;
        void onDetach() override;
        void onEvent(Event& e) override;

        void begin();
        void end();

        void onImGuiRender() override;

        void blockEvents(bool block) { m_blockEvents = block; }
    private:
        bool m_blockEvents = true;
        float m_time = 0.0f;
    };

}

#endif //TEAENGINECOMPLETE_IMGUILAYER_H
