
//
// Created by tim on 21.08.20.
//

#ifndef TEAENGINECOMPLETE_BUFFER_H
#define TEAENGINECOMPLETE_BUFFER_H

#include <utility>
#include <cstdint>
#include <vector>
#include "TeaEngine/Core/Base.h"

class Ref;

namespace teaEngine {
    enum class ShaderDataType {
        None = 0,
        Bool,
        Int,
        Int2,
        Int3,
        Int4,
        Float,
        Float2,
        Float3,
        Float4,
        Mat3,
        Mat4,
    };

    [[nodiscard]] static uint32_t ShaderDataTypeSize(ShaderDataType type) {
        switch (type) {

        case ShaderDataType::None:
            return 0;
        case ShaderDataType::Bool:
            return 1;
        case ShaderDataType::Int:
            return 4;
        case ShaderDataType::Int2:
            return 4 * 2;
        case ShaderDataType::Int3:
            return 4 * 3;
        case ShaderDataType::Int4:
            return 4 * 4;
        case ShaderDataType::Float:
            return 4;
        case ShaderDataType::Float2:
            return 4 * 2;
        case ShaderDataType::Float3:
            return 4 * 3;
        case ShaderDataType::Float4:
            return 4 * 4;
        case ShaderDataType::Mat3:
            return 4 * 3 * 3;
        case ShaderDataType::Mat4:
            return 4 * 4 * 4;
        }

        TE_CORE_ASSERT(false, "Unknown ShaderDataType")
        return 0;
    }

    struct BufferElement {
        std::string name;
        ShaderDataType type;
        u_int32_t size;
        u_int32_t offset;
        bool normalized;
        BufferElement()
        :   name{},
            type{ShaderDataType::None},
            size{0},
            offset{0},
            normalized{false}
        {};
        BufferElement(ShaderDataType type, std::string name, bool normalized = false)
            : name{std::move(name)}, type{type}, size{ShaderDataTypeSize(type)}, offset{0}, normalized{normalized}
        {

        }
        [[nodiscard]] uint32_t getComponentCount() const {
            switch (type) {

            case ShaderDataType::None:
                return 0;
            case ShaderDataType::Bool:
                return 1;
            case ShaderDataType::Int:
                return 1;
            case ShaderDataType::Int2:
                return 2;
            case ShaderDataType::Int3:
                return 3;
            case ShaderDataType::Int4:
                return 4;
            case ShaderDataType::Float:
                return 1;
            case ShaderDataType::Float2:
                return 2;
            case ShaderDataType::Float3:
                return 3;
            case ShaderDataType::Float4:
                return 4;
            case ShaderDataType::Mat3:
                return 3 * 3;
            case ShaderDataType::Mat4:
                return 4 * 4;
            }
        }
    };

    class BufferLayout {
    public:
        BufferLayout()
        : m_elements{}, m_stride{0}
        {
        }
        BufferLayout(const std::initializer_list<BufferElement>& elements)
            : m_elements{elements}, m_stride{0}
        {
            calculateOffsetsAndStride();
        };

        [[nodiscard]] inline const std::vector<BufferElement>& getElements() const { return m_elements; }
        [[nodiscard]] inline uint32_t getStride() const { return m_stride; }

        [[nodiscard]] std::vector<BufferElement>::iterator begin() { return m_elements.begin(); }
        [[nodiscard]] std::vector<BufferElement>::iterator end() { return m_elements.end(); }
        [[nodiscard]] std::vector<BufferElement>::const_iterator begin() const { return m_elements.begin(); }
        [[nodiscard]] std::vector<BufferElement>::const_iterator end() const { return m_elements.end(); }

    private:
        void calculateOffsetsAndStride()
        {
            uint32_t offset = 0;
            m_stride = 0;
            for (auto& element : m_elements) {
                element.offset = offset;
                offset += element.size;
                m_stride += element.size;
            }
        };
    private:
        std::vector<BufferElement> m_elements;
        uint32_t m_stride;
    };

    class VertexBuffer {
    public:
        virtual ~VertexBuffer() = default;

        virtual void bind() const = 0;
        virtual void unbind() const= 0;

        [[nodiscard]] virtual const BufferLayout& getLayout() const = 0;
        virtual void setLayout(const BufferLayout& layout) = 0;
        virtual void setData(const void* data, u_int32_t size) = 0;

        static Ref<VertexBuffer> create(uint32_t size);
        static Ref<VertexBuffer> create(float* vertices, uint32_t size);
    };

    class IndexBuffer {
    public:
        virtual ~IndexBuffer() = default;

        virtual void bind() const = 0;

        virtual void unbind() const = 0;

        [[nodiscard]] virtual u_int32_t getCount() const = 0;

        static Ref<IndexBuffer> create(u_int32_t * indices, uint32_t count);
    };
}


#endif //TEAENGINECOMPLETE_BUFFER_H
