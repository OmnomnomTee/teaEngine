//
// Created by tim on 16.08.20.
//

#ifndef TEAENGINECOMPLETE_GRAPHICSCONTEXT_H
#define TEAENGINECOMPLETE_GRAPHICSCONTEXT_H

#include "TeaEngine/Core/Base.h"
#include <glm/glm.hpp>

namespace teaEngine {

    class GraphicsContext
    {
    public:
        virtual ~GraphicsContext() = default;

        virtual void init() = 0;
        virtual void swapBuffers() = 0;

        static Scope<GraphicsContext> create(void* window);
    };

}

#endif //TEAENGINECOMPLETE_GRAPHICSCONTEXT_H
