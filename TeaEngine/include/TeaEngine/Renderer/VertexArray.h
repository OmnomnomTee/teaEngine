//
// Created by tim on 23.08.20.
//

#ifndef TEAENGINECOMPLETE_VERTEXARRAY_H
#define TEAENGINECOMPLETE_VERTEXARRAY_H

#include "Buffer.h"
#include "TeaEngine/Core/Base.h"

namespace teaEngine{
    class VertexArray {
    public:
        virtual ~VertexArray() = default;

        virtual void bind() const = 0;
        virtual void unbind() const= 0;

        virtual void addVertexBuffer(const Ref<VertexBuffer>& vertexBuffer) = 0;
        virtual void setIndexBuffer(const Ref<IndexBuffer>& indexBuffer) = 0;

        [[nodiscard]] virtual const std::vector<Ref<VertexBuffer>>& getVertexBuffers() const = 0;
        [[nodiscard]] virtual const Ref<IndexBuffer>& getIndexBuffer() const = 0;

        static Ref<VertexArray> create();
    };
}
#endif //TEAENGINECOMPLETE_VERTEXARRAY_H
