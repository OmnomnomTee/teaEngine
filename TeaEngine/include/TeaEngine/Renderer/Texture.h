//
// Created by tim on 29.08.20.
//

#ifndef TEAENGINECOMPLETE_TEXTURE_H
#define TEAENGINECOMPLETE_TEXTURE_H


#include "TeaEngine/Core/Base.h"
#include <memory>

namespace teaEngine {
    class Texture {
    public:
        virtual ~Texture() = default;

        [[nodiscard]] virtual u_int32_t getWidth() const = 0;
        [[nodiscard]] virtual u_int32_t getHeight() const = 0;
        [[nodiscard]] virtual u_int32_t getRendererId() const = 0;

        virtual void bind(u_int32_t  slot) const = 0;

        virtual void setData(void* data, uint32_t size) = 0;

        [[nodiscard]] inline virtual bool operator==(const Texture& other) { return this->getRendererId() == other.getRendererId(); }
    };

    class Texture2D : public Texture {
    public:
        ~Texture2D() override = default;

        static Ref<Texture2D> create(u_int32_t width, u_int32_t height);
        static Ref<Texture2D> create(const std::string& filePath);
    };
}

#endif //TEAENGINECOMPLETE_TEXTURE_H
