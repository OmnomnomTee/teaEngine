//
// Created by tim on 01.09.20.
//

#ifndef TEAENGINECOMPLETE_RENDERER2D_H
#define TEAENGINECOMPLETE_RENDERER2D_H

#include "TeaEngine/Renderer/OrthographicCamera.h"
#include "Shader.h"
#include "Texture.h"
#include "VertexArray.h"
#include <glm/glm.hpp>

namespace teaEngine {
    struct QuadVertex {
        glm::vec3 position = glm::vec3{0.0f };
        glm::vec4 color = glm::vec4{1.0f };
        glm::vec2 texCoord = glm::vec2{0.0f };
        float tilingFactor = 1.0f;
        float texIndex = 1.0f;
    };
    struct Statistics{
        uint32_t drawCalls = 0;
        uint32_t quadCount = 0;

        [[nodiscard]] u_int32_t getVertexCount() const {
            return quadCount * 4;
        }
        [[nodiscard]] u_int32_t getIndexCount() const {
            return quadCount * 6;
        }
    };

    struct Renderer2DStorage {
        static const uint32_t maxQuads = 10000;
        static const uint32_t maxVertices = maxQuads * 4;
        static const uint32_t maxIndices = maxQuads * 6;
        static const u_int32_t maxTextureCount = 24; // TODO make this GPU dependent
        uint32_t indexCount = 0;
        u_int32_t textureSlotIndex = 1;
        Ref<VertexArray> vertexArray;
        Ref<VertexBuffer> vertexBuffer;
        Ref<Shader> shaderTexture;
        Ref<Texture2D> textureWhite;
        std::array<Ref<Texture>, maxTextureCount> textures;
        QuadVertex* vertexBufferBase = nullptr;
        QuadVertex* vertexBufferPtr = nullptr;
        Statistics statistics;
    };

    class Renderer2D {
    public:
        Renderer2D() = default;
        ~Renderer2D() = default;

        static void init();
        static void shutdown();

        static void beginScene(const OrthographicCamera& camera);
        static void endScene();

        // Primitives
        static void drawQuad(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color = glm::vec4{1.0f });
        static void drawQuad(const glm::vec3& position, const glm::vec2& size, const glm::vec4& color = glm::vec4{1.0f });

        static void drawQuad(const glm::vec2& position, const glm::vec2& size, float angle, const glm::vec4& color = glm::vec4{1.0f });
        static void drawQuad(const glm::vec3& position, const glm::vec2& size, float angle, const glm::vec4& color = glm::vec4{1.0f });

        static void drawQuad(const glm::vec2& position, const glm::vec2& size, const Ref<Texture2D>& texture, const glm::vec4& colorTint = glm::vec4{1.0f }, float tilingScale = 1.0f);
        static void drawQuad(const glm::vec3& position, const glm::vec2& size, const Ref<Texture2D>& texture, const glm::vec4& colorTint = glm::vec4{1.0f }, float tilingScale = 1.0f);

        static void drawQuad(const glm::vec2& position, const glm::vec2& size, float angle, const Ref<Texture2D>& texture, const glm::vec4& colorTint = glm::vec4{1.0f }, float tilingScale = 1.0f);
        static void drawQuad(const glm::vec3& position, const glm::vec2& size, float angle, const Ref<Texture2D>& texture, const glm::vec4& colorTint = glm::vec4{1.0f }, float tilingScale = 1.0f);

        // Getters
        [[nodiscard]] inline static Ref<ShaderLibrary> getShaderLibrary() { return m_shaderLibrary; };

        // Debug and Performance
        [[nodiscard]] inline static Statistics getStatistics() {return s_rendererData->statistics; };
        static void resetStatistics();

    private:
        static void flush();

    private:
        static Ref<ShaderLibrary> m_shaderLibrary;
        static Renderer2DStorage* s_rendererData;
    };
}
#endif //TEAENGINECOMPLETE_RENDERER2D_H
