//
// Created by tim on 23.08.20.
//

#ifndef TEAENGINECOMPLETE_RENDERERAPI_H
#define TEAENGINECOMPLETE_RENDERERAPI_H

#include <glm/glm.hpp>
#include "VertexArray.h"

namespace teaEngine {
    class RendererAPI {
    public:
        enum class API {
            None = 0,
            OpenGL = 1,
            Vulkan = 2,
            DirectX12 = 3,
        };

    public:
        virtual ~RendererAPI() = default;

        virtual void init() const= 0;
        virtual void setClearColor(const glm::vec4& color) = 0;
        virtual void setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height) = 0;
        virtual void clear() const= 0;

        virtual void drawIndexed(const Ref<VertexArray>& vertexArray, u_int32_t indexCount) = 0;

        [[nodiscard]] inline static API getAPI() { return s_API; };
    private:
        static API s_API;
    };

}


#endif //TEAENGINECOMPLETE_RENDERERAPI_H
