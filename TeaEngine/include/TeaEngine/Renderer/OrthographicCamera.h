//
// Created by tim on 26.08.20.
//

#ifndef TEAENGINECOMPLETE_ORTHOGRAPHICCAMERA_H
#define TEAENGINECOMPLETE_ORTHOGRAPHICCAMERA_H

#include <glm/glm.hpp>

namespace teaEngine {

    class OrthographicCamera {
    public:
        OrthographicCamera(float left, float right, float bottom, float top);
        OrthographicCamera(float left, float right, float bottom, float top, float zNear, float zFar);

        [[nodiscard]] const glm::vec3& getPosition() const;
        [[nodiscard]] const glm::mat4& getProjectionMatrix() const;
        [[nodiscard]] const glm::mat4& getViewMatrix() const;
        [[nodiscard]] const glm::mat4& getProjectionViewMatrix() const;
        [[nodiscard]] float getRotation() const;

        void setPosition(const glm::vec3& position);
        void setRotation(float rotation);
        void setProjection(float left, float right, float bottom, float top);

    private:
        void recalculateViewMatrix();

    private:
        glm::mat4 m_projectionMatrix;
        glm::mat4 m_viewMatrix;
        glm::mat4 m_projectionViewMatrix;

        glm::vec3 m_position;
        float m_rotation = 0.0f;
    };
}

#endif //TEAENGINECOMPLETE_ORTHOGRAPHICCAMERA_H
