//
// Created by tim on 21.08.20.
//

#ifndef TEAENGINECOMPLETE_RENDERER_H
#define TEAENGINECOMPLETE_RENDERER_H

#include "TeaEngine/Renderer/RendererAPI.h"
#include "TeaEngine/Core/Base.h"
#include "OrthographicCamera.h"
#include "TeaEngine/Renderer/Shader.h"

namespace teaEngine {
    class Renderer {
    public:
        static void beginScene(OrthographicCamera& camera);
        static void endScene();

        static void submit(const Ref<Shader>& shader, const Ref<VertexArray>& vertexArray, const glm::mat4& modelMatrix=glm::mat4(1.0f));
        static void init();

        static void onWindowResize(uint32_t width, uint32_t height);

        [[nodiscard]] inline static RendererAPI::API getAPI() { return RendererAPI::getAPI(); };
        [[nodiscard]] inline static Ref<ShaderLibrary> getShaderLibrary() { return m_shaderLibrary; };

    private:
        struct SceneData {
            glm::mat4 projectionViewMatrix;
        };

        static SceneData* m_sceneData;
        static Ref<ShaderLibrary> m_shaderLibrary;
    };
}

#endif //TEAENGINECOMPLETE_RENDERER_H
