//
// Created by tim on 01.09.20.
//

#ifndef TEAENGINECOMPLETE_ORTHOGRAPHICCAMERACONTROLLER_H
#define TEAENGINECOMPLETE_ORTHOGRAPHICCAMERACONTROLLER_H

#include "TeaEngine/Renderer/OrthographicCamera.h"
#include "TeaEngine/Core/TimeStep.h"
#include "TeaEngine/Events/ApplicationEvent.h"
#include "TeaEngine/Events/MouseEvent.h"

namespace teaEngine {
    struct OrthographicCameraBounds{
        float left;
        float right;
        float top;
        float bottom;
        [[nodiscard]] float getWidth() const {
            return right - left;
        }
        [[nodiscard]] float getHeight() const {
            return top - bottom;
        }
    };

    class OrthographicCameraController {
    public:
        explicit OrthographicCameraController(float aspectRatio);

        void onUpdate(Timestep ts);
        void onEvent(Event& e);

        [[nodiscard]] inline float getAspectRatio() const { return m_aspectRatio; }
        [[nodiscard]] inline float getZoomLevel() const { return m_zoomLevel; }
        [[nodiscard]] inline float getRotation() const { return m_rotation; }
        [[nodiscard]] inline float getRotationSpeed() const { return m_rotationSpeed; }
        [[nodiscard]] inline float getTranslationSpeed() const { return m_translationSpeed; }
        [[nodiscard]] inline const OrthographicCamera& getCamera() const { return m_camera; }
        [[nodiscard]] inline OrthographicCamera& getCamera() { return m_camera; }
        [[nodiscard]] inline OrthographicCameraBounds& getBounds() { return m_cameraBounds; }

    private:
        bool onMouseScrolled(MouseScrolledEvent& event);
        bool onWindowResized(WindowResizeEvent& event);

    private:
        float m_aspectRatio;
        float m_zoomLevel;
        float m_rotation;
        glm::vec3 m_position;
        float m_translationSpeed;
        float m_rotationSpeed;
        OrthographicCameraBounds m_cameraBounds;
        OrthographicCamera m_camera;
    };
}

#endif //TEAENGINECOMPLETE_ORTHOGRAPHICCAMERACONTROLLER_H
