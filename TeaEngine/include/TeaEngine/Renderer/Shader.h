//
// Created by tim on 17.08.20.
//

#ifndef TEAENGINECOMPLETE_SHADER_H
#define TEAENGINECOMPLETE_SHADER_H

#include <string>
#include <unordered_map>
#include <glm/glm.hpp>
#include "TeaEngine/Core/Base.h"

namespace teaEngine {
    class Shader {
    public:
        virtual ~Shader() = default;

        virtual void bind() const = 0;
        virtual void unbind() const = 0;

        [[nodiscard]] virtual const std::string& getName() = 0;

        [[nodiscard]] static Ref<Shader> create(const std:: string& filePath);
        [[nodiscard]] static Ref<Shader> create(const std::string& name, const std:: string& vertexSrc, const std::string& fragmentSrc);

        virtual void uploadUniformInt(const std::string& name, int value) const = 0;
        virtual void uploadUniformInt2(const std::string& name, const glm::vec2& value) const = 0;
        virtual void uploadUniformInt3(const std::string& name, const glm::vec3& value) const = 0;
        virtual void uploadUniformInt4(const std::string& name, const glm::vec4& value) const = 0;

        virtual void uploadUniformIntArray(const std::string& name, int* values, u_int32_t count) const = 0;

        virtual void uploadUniformFloat(const std::string& name, float value) const = 0;
        virtual void uploadUniformFloat2(const std::string& name, const glm::vec2& value) const = 0;
        virtual void uploadUniformFloat3(const std::string& name, const glm::vec3& value) const = 0;
        virtual void uploadUniformFloat4(const std::string& name, const glm::vec4& value) const = 0;

        virtual void uploadUniformMat3(const std::string& name, const glm::mat3& value) const = 0;
        virtual void uploadUniformMat4(const std::string& name, const glm::mat4& value) const = 0;
    };

    class ShaderLibrary {
    public:
        ShaderLibrary() = default;
        ~ShaderLibrary() = default;

        void add(const std::string& name, const Ref<Shader>& shader);
        void add(const Ref<Shader>& shader);

        [[nodiscard]] Ref<Shader> load(const std::string& filePath);
        [[nodiscard]] Ref<Shader> load(const std::string& name, const std::string& filePath);
        [[nodiscard]] Ref<Shader> load(const std::string& name, const std:: string& vertexSrc, const std::string& fragmentSrc);

        [[nodiscard]] Ref<Shader> getShader(const std::string& name);
        [[nodiscard]] bool exists(const std::string& name);
    private:
        std::unordered_map<std::string, Ref<Shader>> m_shaders;
    };
}
#endif //TEAENGINECOMPLETE_SHADER_H
