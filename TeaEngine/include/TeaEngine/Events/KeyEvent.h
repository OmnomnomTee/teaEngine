//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_KEYEVENT_H
#define TEAENGINECOMPLETE_KEYEVENT_H

#include <sstream>
#include "include/TeaEngine/Events/Event.h"
#include "include/TeaEngine/Core/Input.h"

namespace teaEngine {

    class KeyEvent : public Event
    {
    public:
        [[nodiscard]] KeyCode getKeyCode() const { return m_keyCode; }

        EVENT_CLASS_CATEGORY(EventCategoryKeyboard | EventCategoryInput)
    protected:
        explicit KeyEvent(KeyCode keycode, KeyMod keyMod = KeyMod::None)
                : m_keyCode(keycode) {}

        KeyCode m_keyCode;
    };

    class KeyPressedEvent : public KeyEvent
    {
    public:
        KeyPressedEvent(KeyCode keycode, int repeatCount, KeyMod keyMod = KeyMod::None)
                : KeyEvent(keycode, keyMod), m_RepeatCount(repeatCount) {}

        [[nodiscard]] int getRepeatCount() const { return m_RepeatCount; }

        [[nodiscard]] std::string toString() const override
        {
            std::stringstream ss;
            ss << "KeyPressedEvent: " << m_keyCode << " (" << m_RepeatCount << " repeats)";
            return ss.str();
        }

        EVENT_CLASS_TYPE(KeyPressed)
    private:
        int m_RepeatCount;
    };

    class KeyReleasedEvent : public KeyEvent
    {
    public:
        explicit KeyReleasedEvent(KeyCode keycode, KeyMod keyMod = KeyMod::None)
                : KeyEvent(keycode, keyMod) {}

        [[nodiscard]] std::string toString() const override
        {
            std::stringstream ss;
            ss << "KeyReleasedEvent: " << m_keyCode;
            return ss.str();
        }

        EVENT_CLASS_TYPE(KeyReleased)
    };

    class KeyTypedEvent : public KeyEvent
    {
    public:
        explicit KeyTypedEvent(KeyCode keycode, KeyMod keyMod = KeyMod::None)
                : KeyEvent(keycode, keyMod) {}

        [[nodiscard]] std::string toString() const override
        {
            std::stringstream ss;
            ss << "KeyTypedEvent: " << m_keyCode;
            return ss.str();
        }

        EVENT_CLASS_TYPE(KeyTyped)
    };
}

#endif //TEAENGINECOMPLETE_KEYEVENT_H
