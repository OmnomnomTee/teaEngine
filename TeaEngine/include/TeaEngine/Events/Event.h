//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_EVENT_H
#define TEAENGINECOMPLETE_EVENT_H

#include "include/TeaEngine/Core/Base.h"

namespace teaEngine {
    enum class EventType {
        None = 0,
        WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
        AppTick, AppUpdate, AppRender,
        KeyPressed, KeyReleased, KeyTyped,
        MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
    };

    enum EventCategory {
        None = 0,
        EventCategoryApplication    = BIT(0),
        EventCategoryInput          = BIT(1),
        EventCategoryKeyboard       = BIT(2),
        EventCategoryMouse          = BIT(3),
        EventCategoryMouseButton    = BIT(4)
    };

#define EVENT_CLASS_TYPE(type) static EventType getStaticType() { return EventType::type; }\
								virtual EventType getEventType() const override { return getStaticType(); }\
								virtual const char* getName() const override { return #type; }

#define EVENT_CLASS_CATEGORY(category) virtual unsigned int getCategoryFlags() const override { return category; }

    class Event
    {
    public:
        virtual ~Event() = default;

        bool handled = false;

        [[nodiscard]] virtual EventType getEventType() const = 0;
        [[nodiscard]] virtual const char* getName() const = 0;
        [[nodiscard]] virtual unsigned int getCategoryFlags() const = 0;
        [[nodiscard]] virtual std::string toString() const { return getName(); }

        [[nodiscard]] bool isInCategory(EventCategory category) const
        {
            return getCategoryFlags() & category;
        }
    };

    class EventDispatcher
    {
    public:
        explicit EventDispatcher(Event& event)
                : m_Event(event)
        {
        }

        // F will be deduced by the compiler
        template<typename T, typename F>
        bool dispatch(const F& func)
        {
            if (m_Event.getEventType() == T::getStaticType())
            {
                m_Event.handled = func(static_cast<T&>(m_Event));
                return true;
            }
            return false;
        }
    private:
        Event& m_Event;
    };

    inline std::ostream& operator<<(std::ostream& os, const Event& e)
    {
        return os << e.toString();
    }
}

#endif //TEAENGINECOMPLETE_EVENT_H
