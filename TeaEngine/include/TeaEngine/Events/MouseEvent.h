//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_MOUSEEVENT_H
#define TEAENGINECOMPLETE_MOUSEEVENT_H

#include <sstream>
#include "include/TeaEngine/Events/Event.h"
#include "include/TeaEngine/Core/Input.h"

namespace teaEngine {

    class MouseMovedEvent : public Event
    {
    public:
        MouseMovedEvent(float x, float y)
                : m_mouseX(x), m_mouseY(y) {}

        [[nodiscard]] float getX() const { return m_mouseX; }
        [[nodiscard]] float getY() const { return m_mouseY; }

        [[nodiscard]] std::string toString() const override
        {
            std::stringstream ss;
            ss << "MouseMovedEvent: " << m_mouseX << ", " << m_mouseY;
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseMoved)
        EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
    private:
        float m_mouseX, m_mouseY;
    };

    class MouseScrolledEvent : public Event
    {
    public:
        MouseScrolledEvent(float xOffset, float yOffset)
                : m_xOffset(xOffset), m_yOffset(yOffset) {}

        [[nodiscard]] float getXOffset() const { return m_xOffset; }
        [[nodiscard]] float getYOffset() const { return m_yOffset; }

        [[nodiscard]] std::string toString() const override
        {
            std::stringstream ss;
            ss << "MouseScrolledEvent: " << getXOffset() << ", " << getYOffset();
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseScrolled)
        EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
    private:
        float m_xOffset, m_yOffset;
    };

    class MouseButtonEvent : public Event
    {
    public:
        [[nodiscard]] inline MouseCode getMouseButton() const { return m_button; }

        EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
    protected:
        explicit MouseButtonEvent(MouseCode button)
                : m_button(button) {}

        MouseCode m_button;
    };

    class MouseButtonPressedEvent : public MouseButtonEvent
    {
    public:
        explicit MouseButtonPressedEvent(MouseCode button)
                : MouseButtonEvent(button) {}

        [[nodiscard]] std::string toString() const override
        {
            std::stringstream ss;
            ss << "MouseButtonPressedEvent: " << m_button;
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseButtonPressed)
    };

    class MouseButtonReleasedEvent : public MouseButtonEvent
    {
    public:
        explicit MouseButtonReleasedEvent(MouseCode button)
                : MouseButtonEvent(button) {}

        [[nodiscard]] std::string toString() const override
        {
            std::stringstream ss;
            ss << "MouseButtonReleasedEvent: " << m_button;
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseButtonReleased)
    };

}

#endif //TEAENGINECOMPLETE_MOUSEEVENT_H
