//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_TIMESTEP_H
#define TEAENGINECOMPLETE_TIMESTEP_H

#pragma once

namespace teaEngine {

    class Timestep
    {
    public:
        explicit Timestep(float time = 0.0f)
                : m_Time(time)
        {
        }

        explicit operator float() const { return m_Time; }

        [[nodiscard]] float GetSeconds() const { return m_Time; }
        [[nodiscard]] float GetMilliseconds() const { return m_Time * 1000.0f; }
    private:
        float m_Time;
    };

}

#endif //TEAENGINECOMPLETE_TIMESTEP_H
