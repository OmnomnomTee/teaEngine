//
// Created by tim on 16.08.20.
//

#ifndef TEAENGINECOMPLETE_LAYERSTACK_H
#define TEAENGINECOMPLETE_LAYERSTACK_H

#include "TeaEngine//Core/Base.h"
#include "TeaEngine/Core/Layer.h"

#include <vector>

namespace teaEngine {

    class LayerStack
    {
    public:
        LayerStack() = default;
        ~LayerStack();

        void pushLayer(Layer* layer);
        void pushOverlay(Layer* overlay);
        void popLayer(Layer* layer);
        void popOverlay(Layer* overlay);

        [[nodiscard]] std::vector<Layer*>::iterator begin() { return m_layers.begin(); }
        [[nodiscard]] std::vector<Layer*>::iterator end() { return m_layers.end(); }
        [[nodiscard]] std::vector<Layer*>::reverse_iterator rbegin() { return m_layers.rbegin(); }
        [[nodiscard]] std::vector<Layer*>::reverse_iterator rend() { return m_layers.rend(); }

        [[nodiscard]] std::vector<Layer*>::const_iterator begin() const { return m_layers.begin(); }
        [[nodiscard]] std::vector<Layer*>::const_iterator end()	const { return m_layers.end(); }
        [[nodiscard]] std::vector<Layer*>::const_reverse_iterator rbegin() const { return m_layers.rbegin(); }
        [[nodiscard]] std::vector<Layer*>::const_reverse_iterator rend() const { return m_layers.rend(); }
    private:
        std::vector<Layer*> m_layers;
        unsigned int m_layerInsertIndex = 0;
    };

}

#endif //TEAENGINECOMPLETE_LAYERSTACK_H
