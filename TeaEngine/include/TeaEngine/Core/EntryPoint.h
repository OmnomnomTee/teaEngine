//
// Created by tim on 11.01.19.
//

#ifndef PROJECT_ENTRYPOINT_H
#define PROJECT_ENTRYPOINT_H


#include "Application.h"

namespace teaEngine {
    extern teaEngine::Application* teaEngine::createApplication();
}

#endif //PROJECT_ENTRYPOINT_H
