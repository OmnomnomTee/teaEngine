//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_MOUSECODES_H
#define TEAENGINECOMPLETE_MOUSECODES_H

#include <cstdint>
#include <ostream>

namespace teaEngine
{
    typedef enum class MouseCode : uint16_t
    {
        // From glfw3.h
        Button0                = 0,
        Button1                = 1,
        Button2                = 2,
        Button3                = 3,
        Button4                = 4,
        Button5                = 5,
        Button6                = 6,
        Button7                = 7,

        ButtonLast             = Button7,
        ButtonLeft             = Button0,
        ButtonRight            = Button1,
        ButtonMiddle           = Button2
    } Mouse;

    inline std::ostream& operator<<(std::ostream& os, MouseCode mouseCode)
    {
        os << static_cast<int32_t>(mouseCode);
        return os;
    }
}

#define TE_MOUSE_BUTTON_0      ::teaEngine::Mouse::Button0
#define TE_MOUSE_BUTTON_1      ::teaEngine::Mouse::Button1
#define TE_MOUSE_BUTTON_2      ::teaEngine::Mouse::Button2
#define TE_MOUSE_BUTTON_3      ::teaEngine::Mouse::Button3
#define TE_MOUSE_BUTTON_4      ::teaEngine::Mouse::Button4
#define TE_MOUSE_BUTTON_5      ::teaEngine::Mouse::Button5
#define TE_MOUSE_BUTTON_6      ::teaEngine::Mouse::Button6
#define TE_MOUSE_BUTTON_7      ::teaEngine::Mouse::Button7
#define TE_MOUSE_BUTTON_LAST   ::teaEngine::Mouse::ButtonLast
#define TE_MOUSE_BUTTON_LEFT   ::teaEngine::Mouse::ButtonLeft
#define TE_MOUSE_BUTTON_RIGHT  ::teaEngine::Mouse::ButtonRight
#define TE_MOUSE_BUTTON_MIDDLE ::teaEngine::Mouse::ButtonMiddle

#endif //TEAENGINECOMPLETE_MOUSECODES_H
