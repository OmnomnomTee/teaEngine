//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_INPUT_H
#define TEAENGINECOMPLETE_INPUT_H

#include "TeaEngine/Core/Base.h"
#include "TeaEngine/Core/KeyCodes.h"
#include "TeaEngine/Core/MouseCodes.h"

namespace teaEngine {

	class Input
	{
	public:
        [[nodiscard]] static bool isKeyPressed(KeyCode key);

        [[nodiscard]] static bool isMouseButtonPressed(MouseCode button);
        [[nodiscard]] static std::pair<float, float> getMousePosition();
        [[nodiscard]] static float getMouseX();
        [[nodiscard]] static float getMouseY();
	};
}

#endif //TEAENGINECOMPLETE_INPUT_H
