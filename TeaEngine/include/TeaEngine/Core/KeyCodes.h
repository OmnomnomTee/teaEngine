//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_KEYCODES_H
#define TEAENGINECOMPLETE_KEYCODES_H

#include <cstdint>
#include <sstream>

namespace teaEngine
{
    typedef enum class KeyCode : uint16_t
    {
        // From glfw3.h
        Space               = 32,
        Apostrophe          = 39, /* ' */
        Comma               = 44, /* , */
        Minus               = 45, /* - */
        Period              = 46, /* . */
        Slash               = 47, /* / */

        D0                  = 48, /* 0 */
        D1                  = 49, /* 1 */
        D2                  = 50, /* 2 */
        D3                  = 51, /* 3 */
        D4                  = 52, /* 4 */
        D5                  = 53, /* 5 */
        D6                  = 54, /* 6 */
        D7                  = 55, /* 7 */
        D8                  = 56, /* 8 */
        D9                  = 57, /* 9 */

        Semicolon           = 59, /* ; */
        Equal               = 61, /* = */

        A                   = 65,
        B                   = 66,
        C                   = 67,
        D                   = 68,
        E                   = 69,
        F                   = 70,
        G                   = 71,
        H                   = 72,
        I                   = 73,
        J                   = 74,
        K                   = 75,
        L                   = 76,
        M                   = 77,
        N                   = 78,
        O                   = 79,
        P                   = 80,
        Q                   = 81,
        R                   = 82,
        S                   = 83,
        T                   = 84,
        U                   = 85,
        V                   = 86,
        W                   = 87,
        X                   = 88,
        Y                   = 89,
        Z                   = 90,

        LeftBracket         = 91,  /* [ */
        Backslash           = 92,  /* \ */
        RightBracket        = 93,  /* ] */
        GraveAccent         = 96,  /* ` */

        World1              = 161, /* non-US #1 */
        World2              = 162, /* non-US #2 */

        /* Function keys */
        Escape              = 256,
        Enter               = 257,
        Tab                 = 258,
        Backspace           = 259,
        Insert              = 260,
        Delete              = 261,
        Right               = 262,
        Left                = 263,
        Down                = 264,
        Up                  = 265,
        PageUp              = 266,
        PageDown            = 267,
        Home                = 268,
        End                 = 269,
        CapsLock            = 280,
        ScrollLock          = 281,
        NumLock             = 282,
        PrintScreen         = 283,
        Pause               = 284,
        F1                  = 290,
        F2                  = 291,
        F3                  = 292,
        F4                  = 293,
        F5                  = 294,
        F6                  = 295,
        F7                  = 296,
        F8                  = 297,
        F9                  = 298,
        F10                 = 299,
        F11                 = 300,
        F12                 = 301,
        F13                 = 302,
        F14                 = 303,
        F15                 = 304,
        F16                 = 305,
        F17                 = 306,
        F18                 = 307,
        F19                 = 308,
        F20                 = 309,
        F21                 = 310,
        F22                 = 311,
        F23                 = 312,
        F24                 = 313,
        F25                 = 314,

        /* Keypad */
        KP0                 = 320,
        KP1                 = 321,
        KP2                 = 322,
        KP3                 = 323,
        KP4                 = 324,
        KP5                 = 325,
        KP6                 = 326,
        KP7                 = 327,
        KP8                 = 328,
        KP9                 = 329,
        KPDecimal           = 330,
        KPDivide            = 331,
        KPMultiply          = 332,
        KPSubtract          = 333,
        KPAdd               = 334,
        KPEnter             = 335,
        KPEqual             = 336,

        LeftShift           = 340,
        LeftControl         = 341,
        LeftAlt             = 342,
        LeftSuper           = 343,
        RightShift          = 344,
        RightControl        = 345,
        RightAlt            = 346,
        RightSuper          = 347,
        Menu                = 348
    } Key;

    typedef enum class KeyMod : uint16_t {
        None            = 0,
        Shift           = 1,
        Control         = 2,
        Alt             = 3,
        Super           = 4,
        CapsLock        = 6,
        NumLock             = 3,
    } KeyMod;

    inline std::ostream& operator<<(std::ostream& os, KeyCode keyCode)
    {
        os << static_cast<int32_t>(keyCode);
        return os;
    }

    inline std::ostream& operator<<(std::ostream& os, KeyMod KeyMod)
    {
        os << static_cast<int32_t>(KeyMod);
        return os;
    }
}

// From glfw3.h
#define TE_KEY_SPACE           ::teaEngine::Key::Space
#define TE_KEY_APOSTROPHE      ::teaEngine::Key::Apostrophe    /* ' */
#define TE_KEY_COMMA           ::teaEngine::Key::Comma         /* , */
#define TE_KEY_MINUS           ::teaEngine::Key::Minus         /* - */
#define TE_KEY_PERIOD          ::teaEngine::Key::Period        /* . */
#define TE_KEY_SLASH           ::teaEngine::Key::Slash         /* / */
#define TE_KEY_0               ::teaEngine::Key::D0
#define TE_KEY_1               ::teaEngine::Key::D1
#define TE_KEY_2               ::teaEngine::Key::D2
#define TE_KEY_3               ::teaEngine::Key::D3
#define TE_KEY_4               ::teaEngine::Key::D4
#define TE_KEY_5               ::teaEngine::Key::D5
#define TE_KEY_6               ::teaEngine::Key::D6
#define TE_KEY_7               ::teaEngine::Key::D7
#define TE_KEY_8               ::teaEngine::Key::D8
#define TE_KEY_9               ::teaEngine::Key::D9
#define TE_KEY_SEMICOLON       ::teaEngine::Key::Semicolon     /* ; */
#define TE_KEY_EQUAL           ::teaEngine::Key::Equal         /* = */
#define TE_KEY_A               ::teaEngine::Key::A
#define TE_KEY_B               ::teaEngine::Key::B
#define TE_KEY_C               ::teaEngine::Key::C
#define TE_KEY_D               ::teaEngine::Key::D
#define TE_KEY_E               ::teaEngine::Key::E
#define TE_KEY_F               ::teaEngine::Key::F
#define TE_KEY_G               ::teaEngine::Key::G
#define TE_KEY_H               ::teaEngine::Key::H
#define TE_KEY_I               ::teaEngine::Key::I
#define TE_KEY_J               ::teaEngine::Key::J
#define TE_KEY_K               ::teaEngine::Key::K
#define TE_KEY_L               ::teaEngine::Key::L
#define TE_KEY_M               ::teaEngine::Key::M
#define TE_KEY_N               ::teaEngine::Key::N
#define TE_KEY_O               ::teaEngine::Key::O
#define TE_KEY_P               ::teaEngine::Key::P
#define TE_KEY_Q               ::teaEngine::Key::Q
#define TE_KEY_R               ::teaEngine::Key::R
#define TE_KEY_S               ::teaEngine::Key::S
#define TE_KEY_T               ::teaEngine::Key::T
#define TE_KEY_U               ::teaEngine::Key::U
#define TE_KEY_V               ::teaEngine::Key::V
#define TE_KEY_W               ::teaEngine::Key::W
#define TE_KEY_X               ::teaEngine::Key::X
#define TE_KEY_Y               ::teaEngine::Key::Y
#define TE_KEY_Z               ::teaEngine::Key::Z
#define TE_KEY_LEFT_BRACKET    ::teaEngine::Key::LeftBracket   /* [ */
#define TE_KEY_BACKSLASH       ::teaEngine::Key::Backslash     /* \ */
#define TE_KEY_RIGHT_BRACKET   ::teaEngine::Key::RightBracket  /* ] */
#define TE_KEY_GRAVE_ACCENT    ::teaEngine::Key::GraveAccent   /* ` */
#define TE_KEY_WORLD_1         ::teaEngine::Key::World1        /* non-US #1 */
#define TE_KEY_WORLD_2         ::teaEngine::Key::World2        /* non-US #2 */

/* Function keys */
#define TE_KEY_ESCAPE          ::teaEngine::Key::Escape
#define TE_KEY_ENTER           ::teaEngine::Key::Enter
#define TE_KEY_TAB             ::teaEngine::Key::Tab
#define TE_KEY_BACKSPACE       ::teaEngine::Key::Backspace
#define TE_KEY_INSERT          ::teaEngine::Key::Insert
#define TE_KEY_DELETE          ::teaEngine::Key::Delete
#define TE_KEY_RIGHT           ::teaEngine::Key::Right
#define TE_KEY_LEFT            ::teaEngine::Key::Left
#define TE_KEY_DOWN            ::teaEngine::Key::Down
#define TE_KEY_UP              ::teaEngine::Key::Up
#define TE_KEY_PAGE_UP         ::teaEngine::Key::PageUp
#define TE_KEY_PAGE_DOWN       ::teaEngine::Key::PageDown
#define TE_KEY_HOME            ::teaEngine::Key::Home
#define TE_KEY_END             ::teaEngine::Key::End
#define TE_KEY_CAPS_LOCK       ::teaEngine::Key::CapsLock
#define TE_KEY_SCROLL_LOCK     ::teaEngine::Key::ScrollLock
#define TE_KEY_NUM_LOCK        ::teaEngine::Key::NumLock
#define TE_KEY_PRINT_SCREEN    ::teaEngine::Key::PrintScreen
#define TE_KEY_PAUSE           ::teaEngine::Key::Pause
#define TE_KEY_F1              ::teaEngine::Key::F1
#define TE_KEY_F2              ::teaEngine::Key::F2
#define TE_KEY_F3              ::teaEngine::Key::F3
#define TE_KEY_F4              ::teaEngine::Key::F4
#define TE_KEY_F5              ::teaEngine::Key::F5
#define TE_KEY_F6              ::teaEngine::Key::F6
#define TE_KEY_F7              ::teaEngine::Key::F7
#define TE_KEY_F8              ::teaEngine::Key::F8
#define TE_KEY_F9              ::teaEngine::Key::F9
#define TE_KEY_F10             ::teaEngine::Key::F10
#define TE_KEY_F11             ::teaEngine::Key::F11
#define TE_KEY_F12             ::teaEngine::Key::F12
#define TE_KEY_F13             ::teaEngine::Key::F13
#define TE_KEY_F14             ::teaEngine::Key::F14
#define TE_KEY_F15             ::teaEngine::Key::F15
#define TE_KEY_F16             ::teaEngine::Key::F16
#define TE_KEY_F17             ::teaEngine::Key::F17
#define TE_KEY_F18             ::teaEngine::Key::F18
#define TE_KEY_F19             ::teaEngine::Key::F19
#define TE_KEY_F20             ::teaEngine::Key::F20
#define TE_KEY_F21             ::teaEngine::Key::F21
#define TE_KEY_F22             ::teaEngine::Key::F22
#define TE_KEY_F23             ::teaEngine::Key::F23
#define TE_KEY_F24             ::teaEngine::Key::F24
#define TE_KEY_F25             ::teaEngine::Key::F25

/* Keypad */
#define TE_KEY_KP_0            ::teaEngine::Key::KP0
#define TE_KEY_KP_1            ::teaEngine::Key::KP1
#define TE_KEY_KP_2            ::teaEngine::Key::KP2
#define TE_KEY_KP_3            ::teaEngine::Key::KP3
#define TE_KEY_KP_4            ::teaEngine::Key::KP4
#define TE_KEY_KP_5            ::teaEngine::Key::KP5
#define TE_KEY_KP_6            ::teaEngine::Key::KP6
#define TE_KEY_KP_7            ::teaEngine::Key::KP7
#define TE_KEY_KP_8            ::teaEngine::Key::KP8
#define TE_KEY_KP_9            ::teaEngine::Key::KP9
#define TE_KEY_KP_DECIMAL      ::teaEngine::Key::KPDecimal
#define TE_KEY_KP_DIVIDE       ::teaEngine::Key::KPDivide
#define TE_KEY_KP_MULTIPLY     ::teaEngine::Key::KPMultiply
#define TE_KEY_KP_SUBTRACT     ::teaEngine::Key::KPSubtract
#define TE_KEY_KP_ADD          ::teaEngine::Key::KPAdd
#define TE_KEY_KP_ENTER        ::teaEngine::Key::KPEnter
#define TE_KEY_KP_EQUAL        ::teaEngine::Key::KPEqual

#define TE_KEY_LEFT_SHIFT      ::teaEngine::Key::LeftShift
#define TE_KEY_LEFT_CONTROL    ::teaEngine::Key::LeftControl
#define TE_KEY_LEFT_ALT        ::teaEngine::Key::LeftAlt
#define TE_KEY_LEFT_SUPER      ::teaEngine::Key::LeftSuper
#define TE_KEY_RIGHT_SHIFT     ::teaEngine::Key::RightShift
#define TE_KEY_RIGHT_CONTROL   ::teaEngine::Key::RightControl
#define TE_KEY_RIGHT_ALT       ::teaEngine::Key::RightAlt
#define TE_KEY_RIGHT_SUPER     ::teaEngine::Key::RightSuper
#define TE_KEY_MENU            ::teaEngine::Key::

#define TE_KEY_MOD_SHIFT       ::teaEngine::KeyMod::Shift
#define TE_KEY_MOD_CONTROL     ::teaEngine::KeyMod::Control
#define TE_KEY_MOD_ALT         ::teaEngine::KeyMod::Alt
#define TE_KEY_MOD_SUPER       ::teaEngine::KeyMod::Super
#define TE_KEY_MOD_CAPS_LOCK   ::teaEngine::KeyMod::CapsLock
#define TE_KEY_MOD_NUM_LOCK    ::teaEngine::KeyMod::NumLock

#endif //TEAENGINECOMPLETE_KEYCODES_H
