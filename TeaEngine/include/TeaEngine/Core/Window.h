//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_WINDOW_H
#define TEAENGINECOMPLETE_WINDOW_H

#include <utility>
#include <functional>

#include "TeaEngine/Core/Base.h"
#include "TeaEngine/Events/Event.h"

namespace teaEngine {

    struct WindowProps
    {
        std::string title;
        uint32_t width;
        uint32_t height;

        explicit WindowProps(std::string  title = "Tea Engine",
                uint32_t width = 1280,
                uint32_t height = 720)
                : title(std::move(title)), width(width), height(height)
        {
        }
    };

    // Interface representing a desktop system based Window
    class Window
    {
    public:
        using EventCallbackFn = std::function<void(Event&)>;
        virtual ~Window() = default;

        virtual void onUpdate() = 0;

        [[nodiscard]] virtual uint32_t getWidth() const = 0;
        [[nodiscard]] virtual uint32_t getHeight() const = 0;

        // Window attributes
        virtual void setEventCallback(const EventCallbackFn& callback) = 0;
        virtual void setVSync(bool enabled) = 0;

        [[nodiscard]] virtual bool isVSync() const = 0;
        [[nodiscard]] virtual void* getNativeWindow() const = 0;

        [[nodiscard]] static Scope<Window> create(const WindowProps& props = WindowProps());
    };

}

#endif //TEAENGINECOMPLETE_WINDOW_H
