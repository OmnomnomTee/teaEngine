//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_LOG_H
#define TEAENGINECOMPLETE_LOG_H

#include "TeaEngine/Core/Base.h"

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>

namespace teaEngine {

    class Log
    {
    public:
        static void init();

        [[nodiscard]] static Ref<spdlog::logger>& getCoreLogger() { return s_coreLogger; }
        [[nodiscard]] static Ref<spdlog::logger>& getClientLogger() { return s_clientLogger; }
    private:
        static Ref<spdlog::logger> s_coreLogger;
        static Ref<spdlog::logger> s_clientLogger;
    };

}

// Core log macros
#define TE_CORE_TRACE(...)    ::teaEngine::Log::getCoreLogger()->trace(__VA_ARGS__)
#define TE_CORE_INFO(...)     ::teaEngine::Log::getCoreLogger()->info(__VA_ARGS__)
#define TE_CORE_WARN(...)     ::teaEngine::Log::getCoreLogger()->warn(__VA_ARGS__)
#define TE_CORE_ERROR(...)    ::teaEngine::Log::getCoreLogger()->error(__VA_ARGS__)
#define TE_CORE_CRITICAL(...) ::teaEngine::Log::getCoreLogger()->critical(__VA_ARGS__)

// Client log macros
#define TE_TRACE(...)         ::teaEngine::Log::getClientLogger()->trace(__VA_ARGS__)
#define TE_INFO(...)          ::teaEngine::Log::getClientLogger()->info(__VA_ARGS__)
#define TE_WARN(...)          ::teaEngine::Log::getClientLogger()->warn(__VA_ARGS__)
#define TE_ERROR(...)         ::teaEngine::Log::getClientLogger()->error(__VA_ARGS__)
#define TE_CRITICAL(...)      ::teaEngine::Log::getClientLogger()->critical(__VA_ARGS__)

#endif //TEAENGINECOMPLETE_LOG_H
