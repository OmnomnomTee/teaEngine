//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_BASE_H
#define TEAENGINECOMPLETE_BASE_H

#include <memory>

// Platform detection using predefined macros
#ifdef _WIN32
/* Windows x64/x86 */
	#ifdef _WIN64
		/* Windows x64  */
		#define TE_PLATFORM_WINDOWS
	#else
		/* Windows x86 */
		#error "x86 Builds are not supported!"
	#endif
#elif defined(__APPLE__) || defined(__MACH__)
#include <TargetConditionals.h>
	/* TARGET_OS_MAC exists on all the platforms
	 * so we must check all of them (in this order)
	 * to ensure that we're running on MAC
	 * and not some other Apple platform */
	#if TARGET_IPHONE_SIMULATOR == 1
		#error "IOS simulator is not supported!"
	#elif TARGET_OS_IPHONE == 1
		#define HZ_PLATFORM_IOS
		#error "IOS is not supported!"
	#elif TARGET_OS_MAC == 1
		#define HZ_PLATFORM_MACOS
		#error "MacOS is not supported!"
	#else
		#error "Unknown Apple platform!"
	#endif
/* We also have to check __ANDROID__ before __linux__
 * since android is based on the linux kernel
 * it has __linux__ defined */
#elif defined(__ANDROID__)
#define TE_PLATFORM_ANDROID
	#error "Android is not supported!"
#elif defined(__linux__)
    #define TE_PLATFORM_LINUX
#else
/* Unknown compiler/platform */
	#error "Unknown platform!"
#endif // End of platform


#ifdef TE_DEBUG
#if defined(TE_PLATFORM_WINDOWS)
		#define TE_DEBUGBREAK() __debugbreak()
	#elif defined(TE_PLATFORM_LINUX)
		#include <signal.h>
		#define TE_DEBUGBREAK() raise(SIGTRAP)
	#else
		#error "Platform doesn't support debugbreak yet!"
	#endif
	#define TE_ENABLE_ASSERTS
#else
#define TE_DEBUGBREAK()
#endif

// TODO: Make this macro able to take in no arguments except condition
#ifdef TE_ENABLE_ASSERTS
#define TE_ASSERT(x, ...) { if(!(x)) { TE_ERROR("Assertion Failed: {0}", __VA_ARGS__); TE_DEBUGBREAK(); } }
	#define TE_CORE_ASSERT(x, ...) { if(!(x)) { TE_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__); TE_DEBUGBREAK(); } }
#else
#define TE_ASSERT(x, ...)
#define TE_CORE_ASSERT(x, ...)
#endif

#define BIT(x) (1 << x)
#define TE_BIND_EVENT_FN(fn) [this](auto&&... args) -> decltype(auto) { return this->fn(std::forward<decltype(args)>(args)...); }

namespace teaEngine {

    template<typename T>
    using Scope = std::unique_ptr<T>;
    template<typename T, typename ... Args>
    constexpr Scope<T> createScope(Args&& ... args)
    {
        return std::make_unique<T>(std::forward<Args>(args)...);
    }

    template<typename T>
    using Ref = std::shared_ptr<T>;
    template<typename T, typename ... Args>
    constexpr Ref<T> createRef(Args&& ... args)
    {
        return std::make_shared<T>(std::forward<Args>(args)...);
    }

}

#endif //TEAENGINECOMPLETE_BASE_H
