//
// Created by tim on 16.08.20.
//

#ifndef TEAENGINECOMPLETE_LAYER_H
#define TEAENGINECOMPLETE_LAYER_H

#include "TeaEngine/Core/Base.h"
#include "TeaEngine/Core/TimeStep.h"
#include <TeaEngine/Events/Event.h>

namespace teaEngine {
    class Layer {
    public:
        Layer(std::string  name = "Layer");

        virtual ~Layer() = default;

        virtual void onAttach() { }

        virtual void onDetach() { }

        virtual void onUpdate(Timestep ts) { }

        virtual void onImGuiRender() { }

        virtual void onEvent(Event& event) { }

        [[nodiscard]] const std::string& getName() const { return m_debugName; }

    protected:
        std::string m_debugName;
    };
}

#endif //TEAENGINECOMPLETE_LAYER_H
