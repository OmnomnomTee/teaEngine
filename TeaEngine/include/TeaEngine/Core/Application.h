//
// Created by tim on 10.01.19.
//

#ifndef PROJECT_APPLICATION_H
#define PROJECT_APPLICATION_H

#include <TeaEngine/ImGui/ImGuiLayer.h>
#include <TeaEngine/Renderer/Buffer.h>
#include <TeaEngine/Renderer/VertexArray.h>
#include <TeaEngine/Renderer/OrthographicCamera.h>
#include "TeaEngine//Core/Base.h"

#include "include/TeaEngine/Events/ApplicationEvent.h"
#include "TeaEngine/Events/Event.h"
#include "TeaEngine/Core/Window.h"
#include "Layer.h"
#include "LayerStack.h"
#include "include/TeaEngine/Renderer/Shader.h"

int main(int argc, char** argv);

namespace teaEngine{
    class Application {
    public:
        explicit Application(const std::string& name = "Tea Engine");
        virtual ~Application();

        void onEvent(Event& e);

        void pushLayer(Layer* layer);
        void pushOverlay(Layer* layer);

        void close();

        [[nodiscard]] inline Window& getWindow() { return *m_window; }
        [[nodiscard]] inline ImGuiLayer* getImGuiLayer() { return m_imGuiLayer; }
        [[nodiscard]] inline float getLastFrameTime() const { return m_lastFrameTime; }

        [[nodiscard]] static Application& get() { return *s_Instance; }

    private:
        void run();
        bool onWindowClose(WindowCloseEvent& e);
        bool onWindowResize(WindowResizeEvent& e);
    private:
        std::unique_ptr<Window> m_window;
        ImGuiLayer* m_imGuiLayer;
        bool m_running = true;
        bool m_minimized = false;
        LayerStack m_layerStack;
        float m_lastFrameTime = 0.0f;
    private:
        static Application* s_Instance;
        friend int ::main(int argc, char** argv);
    };

    // To be defined in the client
    Application* createApplication();
}

#endif //PROJECT_APPLICATION_H
