//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_TEPH_H
#define TEAENGINECOMPLETE_TEPH_H

// TEPH = Tea Engine Precompiled headers
// The following headers will be pre compiled with help of CMake

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>

#include "TeaEngine/Core/Log.h"
#include "TeaEngine/Core/TimeStep.h"

#endif //TEAENGINECOMPLETE_TEPH_H
