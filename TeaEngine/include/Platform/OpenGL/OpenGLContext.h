//
// Created by tim on 16.08.20.
//

#ifndef TEAENGINECOMPLETE_OPENGLCONTEXT_H
#define TEAENGINECOMPLETE_OPENGLCONTEXT_H


#include "TeaEngine/Renderer/GraphicsContext.h"
#include <glad/glad.h>

struct GLFWwindow;

namespace teaEngine {

    class OpenGLContext : public GraphicsContext
    {
    public:
        OpenGLContext() = default;
        explicit OpenGLContext(GLFWwindow* windowHandle);

        void init() override;
        void swapBuffers() override;

    private:
        static void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message,const GLvoid* userParam);
    private:
        GLFWwindow* m_windowHandle;
    };

}

#endif //TEAENGINECOMPLETE_OPENGLCONTEXT_H
