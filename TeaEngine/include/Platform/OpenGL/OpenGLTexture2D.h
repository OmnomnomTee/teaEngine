//
// Created by tim on 29.08.20.
//

#ifndef TEAENGINECOMPLETE_OPENGLTEXTURE2D_H
#define TEAENGINECOMPLETE_OPENGLTEXTURE2D_H

#include "TeaEngine/Renderer/Texture.h"
#include <glad/glad.h>

namespace teaEngine {
    class OpenGLTexture2D : public Texture2D {
    public:
        explicit OpenGLTexture2D(const std::string& filePath);
        explicit OpenGLTexture2D(u_int32_t width, u_int32_t height);
        ~OpenGLTexture2D() override;

        [[nodiscard]] inline u_int32_t getWidth() const override { return m_width; }
        [[nodiscard]] inline u_int32_t getHeight() const override { return m_height; }
        [[nodiscard]] inline const std::string& getFilePath() const { return m_filePath; }
        [[nodiscard]] inline u_int32_t getRendererId() const override { return m_rendererId; }

        void bind(u_int32_t  slot) const override;

        void setData(void* data, uint32_t size) override;

    private:
        std::string m_filePath;
        uint32_t m_width;
        uint32_t m_height;
        uint32_t m_rendererId;
        GLenum m_internalFormat;
        GLenum m_dataFormat;
    };
}

#endif //TEAENGINECOMPLETE_OPENGLTEXTURE2D_H
