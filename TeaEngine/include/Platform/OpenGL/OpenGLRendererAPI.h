//
// Created by tim on 25.08.20.
//

#ifndef TEAENGINECOMPLETE_OPENGLRENDERERAPI_H
#define TEAENGINECOMPLETE_OPENGLRENDERERAPI_H

#include "TeaEngine/Renderer/RendererAPI.h"
#include "TeaEngine/Core/Base.h"

namespace teaEngine {
    class OpenGLRendererAPI : public RendererAPI{
    public:
        ~OpenGLRendererAPI() override = default;

        void init() const override;

        void setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height) override;

        void setClearColor(const glm::vec4& color) override;

        void clear() const override;

        void drawIndexed(const Ref<VertexArray>& vertexArray, u_int32_t indexCount) override;
    };
}

#endif //TEAENGINECOMPLETE_OPENGLRENDERERAPI_H
