//
// Created by tim on 23.08.20.
//

#ifndef TEAENGINECOMPLETE_OPENGLVERTEXARRAY_H
#define TEAENGINECOMPLETE_OPENGLVERTEXARRAY_H

#include "TeaEngine/Renderer/VertexArray.h"
#include "TeaEngine/Core/Base.h"
#include "TeaEngine/Core/TEPH.h"

namespace teaEngine {
    class OpenGLVertexArray : public VertexArray{
    public:
        OpenGLVertexArray();
        ~OpenGLVertexArray() override;

        void bind() const override;
        void unbind() const override;

        void addVertexBuffer(const Ref<VertexBuffer>& vertexBuffer) override;
        void setIndexBuffer(const Ref<IndexBuffer>& indexBuffer) override;

        const std::vector<Ref<VertexBuffer>>& getVertexBuffers() const override;
        const Ref<IndexBuffer>& getIndexBuffer() const override;

        static VertexArray* create();

    private:
        uint32_t m_rendererId;
        std::vector<Ref<VertexBuffer>> m_vertexBuffers;
        Ref<IndexBuffer> m_indexBuffer;
    };
}

#endif //TEAENGINECOMPLETE_OPENGLVERTEXARRAY_H
