//
// Created by tim on 29.08.20.
//

#ifndef TEAENGINECOMPLETE_OPENGLSHADER_H
#define TEAENGINECOMPLETE_OPENGLSHADER_H

#include <lib/glad/include/glad/glad.h>
#include <glm/glm.hpp>
#include "TeaEngine/Renderer/Shader.h"

namespace teaEngine {
    class OpenGLShader : public Shader {
    public:
        explicit OpenGLShader(const std::string& filePath);
        OpenGLShader(const std::string& name, const std::string& vertexSrc, const std::string& fragmentSrc);
        ~OpenGLShader() override;

        void bind() const override;
        void unbind() const override;

        const std::string& getName() override;

        void uploadUniformInt(const std::string& name, int value) const override;
        void uploadUniformInt2(const std::string& name, const glm::vec2& value) const override;
        void uploadUniformInt3(const std::string& name, const glm::vec3& value) const override;
        void uploadUniformInt4(const std::string& name, const glm::vec4& value) const override;

        void uploadUniformIntArray(const std::string& name, int* value, u_int32_t count) const override;

        void uploadUniformFloat(const std::string& name, float value) const override;
        void uploadUniformFloat2(const std::string& name, const glm::vec2& value) const override;
        void uploadUniformFloat3(const std::string& name, const glm::vec3& value) const override;
        void uploadUniformFloat4(const std::string& name, const glm::vec4& value) const override;

        void uploadUniformMat3(const std::string& name, const glm::mat3& value) const override;
        void uploadUniformMat4(const std::string& name, const glm::mat4& value) const override;

    private:
        std::string readFile(const std::string& filePath);
        std::unordered_map<GLenum, std::string> preprocess(const std::string& source);
        void compile(const std::unordered_map<GLenum, std::string>& shaderSources);

    private:
        uint32_t m_rendererId;
        std::string m_name;
    };
}

#endif //TEAENGINECOMPLETE_OPENGLSHADER_H
