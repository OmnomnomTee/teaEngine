//
// Created by tim on 21.08.20.
//

#ifndef TEAENGINECOMPLETE_OPENGLBUFFER_H
#define TEAENGINECOMPLETE_OPENGLBUFFER_H

#include "include/TeaEngine/Renderer/Buffer.h"

namespace teaEngine {
    class OpenGLVertexBuffer : public VertexBuffer {
    public:
        explicit OpenGLVertexBuffer(u_int32_t size);
        OpenGLVertexBuffer(float* vertices, int32_t size);
        ~OpenGLVertexBuffer() override;

        void bind() const override;
        void unbind() const override;

        [[nodiscard]] inline const BufferLayout& getLayout() const override { return m_layout; }
        inline void setLayout(const BufferLayout& layout) override { m_layout = layout; };

        void setData(const void* data, u_int32_t size) override;

    private:
        uint32_t m_rendererId;
        BufferLayout m_layout;
    };

    class OpenGLIndexBuffer : public IndexBuffer {
    public:
        OpenGLIndexBuffer(u_int32_t * indices, uint32_t count);
        ~OpenGLIndexBuffer() override;

        void bind() const override;
        void unbind() const override;

        [[nodiscard]] u_int32_t getCount() const override;

    private:
        uint32_t m_rendererId;
        uint32_t m_count;
    };
}

#endif //TEAENGINECOMPLETE_OPENGLBUFFER_H
