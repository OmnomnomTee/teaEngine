//
// Created by tim on 15.08.20.
//

#ifndef TEAENGINECOMPLETE_LINUXWINDOW_H
#define TEAENGINECOMPLETE_LINUXWINDOW_H

#include "TeaEngine/Core/Window.h"
#include <GLFW/glfw3.h>
#include <TeaEngine/Renderer/GraphicsContext.h>

namespace teaEngine {

    class WindowsWindow : public Window
    {
    public:
        explicit WindowsWindow(const WindowProps& props);
        ~WindowsWindow() override;

        void onUpdate() override;

        unsigned int getWidth() const override { return m_data.width; }
        unsigned int getHeight() const override { return m_data.height; }

        // Window attributes
        void setEventCallback(const EventCallbackFn& callback) override { m_data.eventCallback = callback; }
        void setVSync(bool enabled) override;
        bool isVSync() const override;

        void centerWindow();

        void* getNativeWindow() const override { return m_window; }
    private:
        virtual void init(const WindowProps& props);
        virtual void shutdown();
    private:
        GLFWwindow* m_window;
        Scope<GraphicsContext> m_context;

        struct WindowData
        {
            std::string title;
            unsigned int width, height;
            bool vSync;

            EventCallbackFn eventCallback;
        };

        WindowData m_data;
    };

}

#endif //TEAENGINECOMPLETE_LINUXWINDOW_H
