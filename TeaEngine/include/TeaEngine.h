//
// Created by tim on 16.08.20.
//

#ifndef TEAENGINECOMPLETE_TEAENGINE_H
#define TEAENGINECOMPLETE_TEAENGINE_H

// For use by TeaEngine applications

// ------------------------- Libraries -------------------------
#include <imgui.h>
#include <lib/ImGuiFileDialog/include/ImGuiFileDialog.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/compatibility.hpp>

// ------------------------- Core Headers -------------------------
#include "TeaEngine/Core/Base.h"

#include "TeaEngine/Core/Application.h"
#include "TeaEngine/Core/Layer.h"
#include "TeaEngine/Core/Log.h"

#include "TeaEngine/Core/TimeStep.h"

#include "TeaEngine/Core/Input.h"
#include "TeaEngine/Core/KeyCodes.h"
#include "TeaEngine/Core/MouseCodes.h"

#include "include/TeaEngine/Renderer/OrthographicCameraController.h"

// ------------------------- Renderer -------------------------
#include "TeaEngine/Renderer/Renderer.h"
#include "TeaEngine/Renderer/Renderer2D.h"
#include "TeaEngine/Renderer/RenderCommand.h"
#include "TeaEngine/Renderer/Shader.h"
#include "TeaEngine/Renderer/VertexArray.h"
#include "TeaEngine/Renderer/Buffer.h"
#include "TeaEngine/Renderer/OrthographicCamera.h"
#include "TeaEngine/Renderer/Texture.h"
#include "Platform/OpenGL/OpenGLShader.h"

#include "TeaEngine/ImGui/ImGuiLayer.h"

// ------------------------- Entry Point -------------------------
#include "TeaEngine/Core/EntryPoint.h"

#endif //TEAENGINECOMPLETE_TEAENGINE_H
