//
// Created by tim on 23.08.20.
//

#include "Platform/OpenGL/OpenGLVertexArray.h"
#include <glad/glad.h>

namespace teaEngine {
    static GLenum ShaderDataTypeToOpenGLType(ShaderDataType type) {
        switch (type) {

        case ShaderDataType::None:
            break;
        case ShaderDataType::Bool:
            return GL_BOOL;
        case ShaderDataType::Int:
            return GL_INT;
        case ShaderDataType::Int2:
            return GL_INT;
        case ShaderDataType::Int3:
            return GL_INT;
        case ShaderDataType::Int4:
            return GL_INT;
        case ShaderDataType::Float:
            return GL_FLOAT;
        case ShaderDataType::Float2:
            return GL_FLOAT;
        case ShaderDataType::Float3:
            return GL_FLOAT;
        case ShaderDataType::Float4:
            return GL_FLOAT;
        case ShaderDataType::Mat3:
            return GL_FLOAT;
        case ShaderDataType::Mat4:
            return GL_FLOAT;
        }

        TE_CORE_ASSERT(false, "Unknown ShaderDataType")
        return 0;
    }

    OpenGLVertexArray::OpenGLVertexArray()
        : m_rendererId{0}
    {
        glCreateVertexArrays(1, &m_rendererId);
    }

    OpenGLVertexArray::~OpenGLVertexArray()
    {
        glDeleteVertexArrays(1,&m_rendererId);
    }

    void OpenGLVertexArray::bind() const
    {
        glBindVertexArray(m_rendererId);
    }

    void OpenGLVertexArray::unbind() const
    {
        glBindVertexArray(0);
    }

    void OpenGLVertexArray::addVertexBuffer(const Ref<VertexBuffer>& vertexBuffer)
    {
        glBindVertexArray(m_rendererId);
        vertexBuffer->bind();

        uint32_t index = 0;
        for(const auto& element : vertexBuffer->getLayout()) {
            glEnableVertexAttribArray(index);
            glVertexAttribPointer(
                    index,
                    element.getComponentCount(),
                    ShaderDataTypeToOpenGLType(element.type),
                    element.normalized? GL_TRUE : GL_FALSE,
                    vertexBuffer->getLayout().getStride(),
                    reinterpret_cast<const void*>(element.offset));
            index++;
        }

        m_vertexBuffers.push_back(vertexBuffer);
    }

    void OpenGLVertexArray::setIndexBuffer(const Ref<IndexBuffer>& indexBuffer)
    {
        glBindVertexArray(m_rendererId);
        indexBuffer->bind();
        m_indexBuffer = indexBuffer;
    }

    const std::vector<Ref<VertexBuffer>>& OpenGLVertexArray::getVertexBuffers() const
    {
        return m_vertexBuffers;
    }

    const Ref<IndexBuffer>& OpenGLVertexArray::getIndexBuffer() const
    {
        return m_indexBuffer;
    }


}