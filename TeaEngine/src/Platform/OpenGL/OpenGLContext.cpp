//
// Created by tim on 16.08.20.
//


#include "Platform/OpenGL/OpenGLContext.h"
#include "TeaEngine/Core/Log.h"

#include <GLFW/glfw3.h>
#include <glad/glad.h>

namespace teaEngine {

    OpenGLContext::OpenGLContext(GLFWwindow* windowHandle)
            : m_windowHandle(windowHandle)
    {
        TE_CORE_ASSERT(windowHandle, "Window handle is null!")
    }

    void OpenGLContext::init()
    {
        glfwMakeContextCurrent(m_windowHandle);
        int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
        TE_CORE_ASSERT(status, "Failed to initialize Glad!");

        TE_CORE_INFO("OpenGL Info:");
        TE_CORE_INFO("  Vendor: {0}", glGetString(GL_VENDOR));
        TE_CORE_INFO("  Renderer: {0}", glGetString(GL_RENDERER));
        TE_CORE_INFO("  Version: {0}", glGetString(GL_VERSION));

#ifdef TE_ENABLE_ASSERTS
        int versionMajor;
		int versionMinor;
		glGetIntegerv(GL_MAJOR_VERSION, &versionMajor);
		glGetIntegerv(GL_MINOR_VERSION, &versionMinor);

		TE_CORE_ASSERT(versionMajor > 4 || (versionMajor == 4 && versionMinor >= 5), "TeaEngine requires at least OpenGL version 4.5!");
#endif

        // region Enable error handling
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(openglCallbackFunction, nullptr);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
        // endregion
    }

    void OpenGLContext::swapBuffers()
    {
        glfwSwapBuffers(m_windowHandle);
    }

    /**
 * Error Handling
 * <a href="https://www.khronos.org/opengl/wiki/Debug_Output">link Documentation</a>
 * <a href="https://github.com/SaschaWillems/openglcpp/blob/master/computeShader/computeShaderParticleSystem/main.cpp">link Source</a>
 * @param source
 * @param type
 * @param id
 * @param severity
 * @param length
 * @param message
 * @param userParam
 */
    void APIENTRY OpenGLContext::openglCallbackFunction(GLenum source, GLenum type, GLuint id, GLenum severity,
            GLsizei length, const GLchar* message, const GLvoid* userParam)
    {
        std::string msgSource;
        switch (source){
        case GL_DEBUG_SOURCE_API:
            msgSource = "WINDOW_SYSTEM";
            break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            msgSource = "SHADER_COMPILER";
            break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            msgSource = "THIRD_PARTY";
            break;
        case GL_DEBUG_SOURCE_APPLICATION:
            msgSource = "APPLICATION";
            break;
        case GL_DEBUG_SOURCE_OTHER:
            msgSource = "OTHER";
            break;
        default:
            msgSource = "UNHANDLED SWITCH CASE";
            break;
        }

        std::string msgType;
        switch (type) {
        case GL_DEBUG_TYPE_ERROR:
            msgType = "ERROR";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            msgType = "DEPRECATED_BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            msgType = "UNDEFINED_BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            msgType = "PORTABILITY";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            msgType = "PERFORMANCE";
            break;
        case GL_DEBUG_TYPE_OTHER:
            msgType = "OTHER";
            break;
        default:
            msgType = "UNHANDLED SWITCH CASE";
            break;
        }

        std::string msgSeverity;
        switch (severity){
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            msgSeverity = "NOTIFICATION";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            msgSeverity = "LOW";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            msgSeverity = "MEDIUM";
            break;
        case GL_DEBUG_SEVERITY_HIGH:
            msgSeverity = "HIGH";
            break;
        default:
            msgSeverity = "UNHANDLED SWITCH CASE";
            break;
        }

        printf("glDebugMessage:\n%s \n type = %s source = %s severity = %s\n", message, msgType.c_str(), msgSource.c_str(), msgSeverity.c_str());
    }

}