//
// Created by tim on 29.08.20.
//

#include "Platform/OpenGL/OpenGLTexture2D.h"
#include <glad/glad.h>
#include <stb/stb_image.h>

namespace teaEngine {
    OpenGLTexture2D::OpenGLTexture2D(const std::string& filePath)
        : m_filePath{filePath},
        m_width{0},
        m_height{0},
        m_rendererId{0},
        m_internalFormat{0},
        m_dataFormat{0}
{
        int width, height, channels;
        stbi_set_flip_vertically_on_load(true);
        stbi_uc* data = stbi_load(filePath.c_str(), &width, &height, &channels, 0);
        TE_ASSERT(data != nullptr, "Could not load file")

        m_width = width;
        m_height = height;

        if(channels == 3) {
            m_internalFormat = GL_RGB8;
            m_dataFormat = GL_RGB;
        }
        else if (channels == 4) {
            m_internalFormat = GL_RGBA8;
            m_dataFormat = GL_RGBA;
        }
        TE_ASSERT(internalFormat & dataFormat, "File format not supported!")

        glCreateTextures(GL_TEXTURE_2D, 1, &m_rendererId);
        glTextureStorage2D(m_rendererId, 1, m_internalFormat, m_width, m_height);

        glTextureParameteri(m_rendererId, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTextureParameteri(m_rendererId, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTextureParameteri(m_rendererId, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTextureParameteri(m_rendererId, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTextureSubImage2D(m_rendererId, 0, 0, 0, m_width, m_height, m_dataFormat, GL_UNSIGNED_BYTE, data);

        stbi_image_free(data);
    }

    OpenGLTexture2D::OpenGLTexture2D(u_int32_t width, u_int32_t height)
        :
        m_filePath{},
        m_width{ width },
        m_height{ height },
        m_rendererId{0},
        m_internalFormat{0},
        m_dataFormat{0}
    {
        m_internalFormat = GL_RGBA8;
        m_dataFormat = GL_RGBA;

        glCreateTextures(GL_TEXTURE_2D, 1, &m_rendererId);
        glTextureStorage2D(m_rendererId, 1, m_internalFormat, m_width, m_height);

        glTextureParameteri(m_rendererId, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTextureParameteri(m_rendererId, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTextureParameteri(m_rendererId, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTextureParameteri(m_rendererId, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    OpenGLTexture2D::~OpenGLTexture2D()
    {
        glDeleteTextures(1, &m_rendererId);
    }

    void teaEngine::OpenGLTexture2D::bind(u_int32_t slot) const
    {
        glBindTextureUnit(slot, m_rendererId);
    }

    void OpenGLTexture2D::setData(void* data, uint32_t size)
    {
        glTextureSubImage2D(m_rendererId, 0, 0, 0, m_width, m_height, m_dataFormat, GL_UNSIGNED_BYTE, data);
    }

}
