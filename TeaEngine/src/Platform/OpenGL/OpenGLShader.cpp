//
// Created by tim on 17.08.20.
//

#include "Platform/OpenGL/OpenGLShader.h"
#include "TeaEngine/Core/Log.h"

#include <glm/glm.hpp>
#include <glad/glad.h>
#include <fstream>
#include <filesystem>
#include <glm/gtc/type_ptr.hpp>

namespace teaEngine {
    static GLenum shaderTypeFromString(const std::string& type) {
        if(type == "vertex") return GL_VERTEX_SHADER;
        else if(type == "fragment") return GL_FRAGMENT_SHADER;

        TE_CORE_ASSERT(false, "Unknown shader type");
        return 0;
    }

    OpenGLShader::OpenGLShader(const std::string& filePath)
        : m_rendererId{0}
    {
        // Read the file and split it into different shader types
        std::string shaderSrc = readFile(filePath);
        auto shaderSources = preprocess(shaderSrc);

        // Compile the shaders and link it to the program
        compile(shaderSources);

        // Returns the file's name stripped of the extension.
        std::filesystem::path path = filePath;
        m_name = path.stem().string();
    }

    OpenGLShader::OpenGLShader(const std::string& name, const std::string& vertexSrc, const std::string& fragmentSrc)
        : m_rendererId{0}
    {
        std::unordered_map<GLenum, std::string> shaderSources;
        shaderSources[GL_VERTEX_SHADER] = vertexSrc;
        shaderSources[GL_FRAGMENT_SHADER] = fragmentSrc;
        compile(shaderSources);

        m_name = name;
    }

    OpenGLShader::~OpenGLShader()
    {
        glDeleteProgram(m_rendererId);
    }

    std::string OpenGLShader::readFile(const std::string& filePath)
    {
        std::string result;
        std::ifstream in(filePath, std::ios::in | std::ios::binary);

        if(in)
        {
            in.seekg(0, std::ios::end);
            result.resize(in.tellg());
            in.seekg(0, std::ios::beg);
            in.read(&result[0], result.size());
            in.close();
        }
        else {
            TE_CORE_ERROR("Could not open file '{0}", filePath);
        }

        return result;
    }

    std::unordered_map<GLenum, std::string> OpenGLShader::preprocess(const std::string& source)
    {
        const char* typeToken = "#type";
        size_t typeTokenLength = strlen(typeToken);
        std::unordered_map<GLenum, std::string> shaderSources;

        //  source.find(typeToken, 0) will EITHER return the position of the first appearance of "#type"
        //  if '#type' appears at least once in the source,
        //  OR it will return a value that is equal to std::string::npos if '#type' does not appear at all in the source.
        size_t pos = source.find(typeToken, 0);
        while(pos != std::string::npos) {
            size_t eol = source.find_first_of("\r\n", pos);
            TE_CORE_ASSERT(eol != std::string::npos, "Syntax error");

            size_t begin = pos + typeTokenLength + 1;
            GLenum type = (shaderTypeFromString(source.substr(begin, eol - begin)));
            TE_CORE_ASSERT(type != 0, "Invalid shader type");

            size_t nextLinePos = source.find_first_of("\r\n", pos);
            pos = source.find(typeToken, nextLinePos);
            shaderSources[type] = source.substr(nextLinePos, pos - (nextLinePos == std::string::npos ? source.size() - 1 : nextLinePos));
        }

        return shaderSources;
    }

    void OpenGLShader::compile(const std::unordered_map<GLenum, std::string>& shaderSources)
    {
        GLuint programId = glCreateProgram();
        std::vector<GLenum> shaderIds;
        shaderIds.reserve(shaderSources.size());

        // Compile shaders
        for (auto&& [key, value] : shaderSources)
        {
            // Create an empty shader handle
            GLuint shader = glCreateShader(key);

            TE_CORE_ASSERT(shader != 0, "Could not create shader")
            shaderIds.push_back(shader);

            // Send the vertex shader source code to GL
            // Note that std::string's .c_str is NULL character terminated.
            const auto* source = static_cast<const GLchar*>( value.c_str());
            glShaderSource(shader, 1, &source, nullptr);

            // Compile the shader
            glCompileShader(shader);

            GLint isCompiled = 0;
            glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
            if (isCompiled==GL_FALSE) {
                GLint maxLength = 0;
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

                // The maxLength includes the NULL character
                std::vector<GLchar> infoLog(maxLength);
                glGetShaderInfoLog(shader, maxLength, &maxLength, &infoLog[0]);

                // We don't need the shader anymore.
                glDeleteShader(shader);

                // Use the infoLog as you see fit.
                TE_CORE_ERROR("{0}", infoLog.data());
                TE_CORE_ASSERT(false, "Could not compile shader")

                // In this simple program, we'll just leave
                return;
            }

            glAttachShader(programId, shader);
        }

        // Link our program
        glLinkProgram(programId);

        // Note the different functions here: glGetProgram* instead of glGetShader*.
        GLint isLinked = 0;
        glGetProgramiv(programId, GL_LINK_STATUS, static_cast<int*>( &isLinked));
        if (isLinked==GL_FALSE) {
            GLint maxLength = 0;
            glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &maxLength);

            // The maxLength includes the NULL character
            std::vector<GLchar> infoLog(maxLength);
            glGetProgramInfoLog(programId, maxLength, &maxLength, &infoLog[0]);

            // We don't need the program anymore.
            glDeleteProgram(programId);

            // Don't leak shaders either.
            for(auto id: shaderIds) {
                glDeleteShader(id);
            }

            // Use the infoLog as you see fit.
            TE_CORE_ERROR("{0}", infoLog.data());
            TE_CORE_ASSERT(false, "Could not link program")

            // In this simple program, we'll just leave
            return;
        }

        // Always detach shaders after a successful link.
        for(auto id: shaderIds) {
            glDetachShader(programId, id);
        }

        // Everything works, now we can assign our shader
        m_rendererId = programId;

        TE_CORE_INFO("{0}", "Compiled Shader successfully");
    }

    void OpenGLShader::bind() const
    {
        glUseProgram(m_rendererId);
    }

    void OpenGLShader::unbind() const
    {
        glUseProgram(0);
    }

    void OpenGLShader::uploadUniformInt(const std::string& name, int value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform1i(location, value);
    }

    void OpenGLShader::uploadUniformInt2(const std::string& name, const glm::vec2& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform2i(location, value.x, value.y);
    }

    void OpenGLShader::uploadUniformInt3(const std::string& name, const glm::vec3& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform3i(location, value.x, value.y, value.z);
    }

    void OpenGLShader::uploadUniformInt4(const std::string& name, const glm::vec4& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform4i(location, value.x, value.y, value.z, value.w);
    }

    void OpenGLShader::uploadUniformFloat(const std::string& name, float value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform1f(location, value);
    }

    void OpenGLShader::uploadUniformFloat2(const std::string& name, const glm::vec2& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform2f(location, value.x, value.y);
    }

    void OpenGLShader::uploadUniformFloat3(const std::string& name, const glm::vec3& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform3f(location, value.x, value.y, value.z);
    }

    void OpenGLShader::uploadUniformFloat4(const std::string& name, const glm::vec4& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform4f(location, value.x, value.y, value.z, value.w);
    }

    void OpenGLShader::uploadUniformMat3(const std::string& name, const glm::mat3& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(value));
    }

    void OpenGLShader::uploadUniformMat4(const std::string& name, const glm::mat4& value) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
    }

    const std::string& OpenGLShader::getName()
    {
        return m_name;
    }

    void OpenGLShader::uploadUniformIntArray(const std::string& name, int* values, u_int32_t count) const
    {
        GLint location = glGetUniformLocation(m_rendererId, name.c_str());
        glUniform1iv(location, count, values);
    }

}
