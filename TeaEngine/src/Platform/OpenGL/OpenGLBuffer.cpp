//
// Created by tim on 21.08.20.
//

#include "Platform/OpenGL/OpenGLBuffer.h"
#include <glad/glad.h>

namespace teaEngine {

    // ------------------------- Vertex Buffer ------------------------
    OpenGLVertexBuffer::OpenGLVertexBuffer(float* vertices, int32_t size)
        : m_rendererId{0}
    {
        glCreateBuffers(1, &m_rendererId);
        glBindBuffer(GL_ARRAY_BUFFER, m_rendererId);
        glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
    }

    OpenGLVertexBuffer::OpenGLVertexBuffer(u_int32_t size)
        : m_rendererId{0}
    {
        glCreateBuffers(1, &m_rendererId);
        glBindBuffer(GL_ARRAY_BUFFER, m_rendererId);
        glBufferData(GL_ARRAY_BUFFER, size, nullptr, GL_DYNAMIC_DRAW);
    }

    OpenGLVertexBuffer::~OpenGLVertexBuffer()
    {
        glDeleteBuffers(1, &m_rendererId);
    }

    void OpenGLVertexBuffer::bind() const
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_rendererId);
    }

    void OpenGLVertexBuffer::unbind() const
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void OpenGLVertexBuffer::setData(const void* data, u_int32_t size)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_rendererId);
        glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);
    }

    // ------------------------- Index Buffer ------------------------
    OpenGLIndexBuffer::OpenGLIndexBuffer(u_int32_t * indices, uint32_t count)
        : m_rendererId(0), m_count{count}
    {
        glCreateBuffers(1, &m_rendererId);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_rendererId);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(u_int32_t), indices, GL_STATIC_DRAW);
    }

    OpenGLIndexBuffer::~OpenGLIndexBuffer()
    {
        glDeleteBuffers(1, &m_rendererId);
    }

    void OpenGLIndexBuffer::bind() const
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_rendererId);
    }

    void OpenGLIndexBuffer::unbind() const
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    u_int32_t OpenGLIndexBuffer::getCount() const
    {
        return m_count;
    }


}
