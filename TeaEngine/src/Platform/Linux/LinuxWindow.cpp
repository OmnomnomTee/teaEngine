//
// Created by tim on 15.08.20.
//


#include "Platform/Linux/LinuxWindow.h"
#include "TeaEngine/Core/Input.h"

#include "TeaEngine/Events/ApplicationEvent.h"
#include "TeaEngine/Events/MouseEvent.h"
#include "TeaEngine/Events/KeyEvent.h"
#include "TeaEngine/Core/Base.h"
#include "TeaEngine/Core/Log.h"

#include <glad/glad.h>

namespace teaEngine {

    static uint8_t s_GLFWWindowCount = 0;

    static void GLFWErrorCallback(int error, const char* description)
    {
        TE_CORE_ERROR("GLFW Error ({0}): {1}", error, description);
    }

    LinuxWindow::LinuxWindow(const WindowProps& props)
        : m_window{nullptr}
    {
//        TE_PROFILE_FUNCTION();

        init(props);
    }

    LinuxWindow::~LinuxWindow()
    {
//        TE_PROFILE_FUNCTION();

        shutdown();
    }

    void LinuxWindow::init(const WindowProps& props)
    {
//        TE_PROFILE_FUNCTION();

        m_data.title = props.title;
        m_data.width = props.width;
        m_data.height = props.height;

        TE_CORE_INFO("Creating window {0} ({1}, {2})", props.title, props.width, props.height);

        if (s_GLFWWindowCount==0) {
            int success = glfwInit();
            TE_CORE_ASSERT(success, "Could not initialize GLFW!");
            glfwSetErrorCallback(GLFWErrorCallback);
        }

        {
#if defined(TE_DEBUG)
            glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif

            glfwWindowHint(GLFW_FOCUSED, GLFW_TRUE);
            glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE);
            m_window = glfwCreateWindow(static_cast<int>(props.width), static_cast<int>(props.height), m_data.title.c_str(), nullptr, nullptr);
            ++s_GLFWWindowCount;
        }

        m_context = GraphicsContext::create(m_window);
        m_context->init();

        glfwSetWindowUserPointer(m_window, &m_data);
        setVSync(true);
        glfwSetInputMode(m_window, GLFW_LOCK_KEY_MODS, GLFW_TRUE);

        // Set GLFW callbacks
        glfwSetWindowSizeCallback(m_window, [](GLFWwindow* window, int width, int height) {
            WindowData& data = *reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));
            data.width = width;
            data.height = height;

            WindowResizeEvent event(width, height);
            data.eventCallback(event);
        });

        glfwSetWindowCloseCallback(m_window, [](GLFWwindow* window) {
            WindowData& data = *reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));
            WindowCloseEvent event;
            data.eventCallback(event);
        });

        glfwSetKeyCallback(m_window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
            WindowData& data = *reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            // If GLFW lacks a key token for it, for example E-mail and Play keys
            if(key == GLFW_KEY_UNKNOWN) {
                TE_ERROR("{0}", "Unknown key pressed");
                return;
            }

            switch (action) {
            case GLFW_PRESS: {
                KeyPressedEvent event(static_cast<KeyCode>(key), 0, static_cast<KeyMod>(mods));
                data.eventCallback(event);
                break;
            }
            case GLFW_RELEASE: {
                KeyReleasedEvent event(static_cast<KeyCode>(key), static_cast<KeyMod>(mods));
                data.eventCallback(event);
                break;
            }
            case GLFW_REPEAT: {
                KeyPressedEvent event(static_cast<KeyCode>(key), 1, static_cast<KeyMod>(mods));
                data.eventCallback(event);
                break;
            }
            default: {
                break;
            }
            }
        });

        glfwSetCharCallback(m_window, [](GLFWwindow* window, unsigned int keycode) {
            WindowData& data = *reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            KeyTypedEvent event(static_cast<KeyCode>(keycode));
            data.eventCallback(event);
        });

        glfwSetMouseButtonCallback(m_window, [](GLFWwindow* window, int button, int action, int mods) {
            WindowData& data = *reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            switch (action) {
            case GLFW_PRESS: {
                MouseButtonPressedEvent event(static_cast<MouseCode>(button));
                data.eventCallback(event);
                break;
            }
            case GLFW_RELEASE: {
                MouseButtonReleasedEvent event(static_cast<MouseCode>(button));
                data.eventCallback(event);
                break;
            }
            default:
                break;
            }
        });

        glfwSetScrollCallback(m_window, [](GLFWwindow* window, double xOffset, double yOffset) {
            WindowData& data = *reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            MouseScrolledEvent event(static_cast<float>( xOffset), static_cast<float>(yOffset));
            data.eventCallback(event);
        });

        glfwSetCursorPosCallback(m_window, [](GLFWwindow* window, double xPos, double yPos) {
            WindowData& data = *reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            MouseMovedEvent event(static_cast<float>( xPos), static_cast<float>( yPos));
            data.eventCallback(event);
        });
    }

    void LinuxWindow::shutdown()
    {
//        TE_PROFILE_FUNCTION();

        glfwDestroyWindow(m_window);
        --s_GLFWWindowCount;

        if (s_GLFWWindowCount==0) {
            glfwTerminate();
        }
    }

    void LinuxWindow::onUpdate()
    {
        glfwPollEvents();
        m_context->swapBuffers();
    }

    void LinuxWindow::setVSync(bool enabled)
    {
        if (enabled)
            glfwSwapInterval(1);
        else
            glfwSwapInterval(0);

        m_data.vSync = enabled;
    }

    bool LinuxWindow::isVSync() const
    {
        return m_data.vSync;
    }

    void LinuxWindow::centerWindow()
    {
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();

        if (!monitor)
            return;

        const GLFWvidmode *mode = glfwGetVideoMode(monitor);
        if (!mode)
            return;

        int monitorX, monitorY;
        glfwGetMonitorPos(monitor, &monitorX, &monitorY);

        int windowWidth, windowHeight;
        glfwGetWindowSize(m_window, &windowWidth, &windowHeight);

        glfwSetWindowPos(m_window,
                monitorX + (mode->width - windowWidth) / 2,
                monitorY + (mode->height - windowHeight) / 2);
    }

}
