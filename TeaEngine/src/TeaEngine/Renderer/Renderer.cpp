//
// Created by tim on 21.08.20.
//

#include "TeaEngine/Renderer/Shader.h"
#include "Platform/OpenGL/OpenGLShader.h"
#include "TeaEngine/Renderer/Renderer.h"
#include "TeaEngine/Renderer/RendererAPI.h"
#include "TeaEngine/Renderer/RenderCommand.h"

namespace teaEngine {
    Renderer::SceneData* Renderer::m_sceneData = new Renderer::SceneData();
    Ref<ShaderLibrary> Renderer::m_shaderLibrary = createRef<ShaderLibrary>();

    void Renderer::beginScene(OrthographicCamera& camera)
    {
        m_sceneData->projectionViewMatrix = camera.getProjectionViewMatrix();
    }

    void Renderer::endScene()
    {

    }

    void Renderer::submit(const Ref<Shader>& shader, const Ref<VertexArray>& vertexArray, const glm::mat4& modelMatrix)
    {
        // TODO make this type save for other APIs than OpenGL
        std::dynamic_pointer_cast<OpenGLShader>(shader)->bind();
        std::dynamic_pointer_cast<OpenGLShader>(shader)->uploadUniformMat4( "u_projectionView", m_sceneData->projectionViewMatrix);
        std::dynamic_pointer_cast<OpenGLShader>(shader)->uploadUniformMat4("u_modelMatrix", modelMatrix);

        vertexArray->bind();
        RenderCommand::drawIndexed(vertexArray, 0);
    }

    void Renderer::init()
    {
        RenderCommand::init();
    }

    void Renderer::onWindowResize(uint32_t width, uint32_t height)
    {
        RenderCommand::setViewport(0, 0, width, height);
    }
}

