//
// Created by tim on 29.08.20.
//

#include "TeaEngine/Renderer/Renderer.h"
#include "TeaEngine/Renderer/Texture.h"
#include "Platform/OpenGL/OpenGLTexture2D.h"

#include <string>

namespace teaEngine {
    Ref<Texture2D> Texture2D::create(const std::string& filePath) {
        switch(Renderer::getAPI()) {

        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLTexture2D>(filePath);
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        }

        TE_CORE_ASSERT(false, "Unknown renderer API")
        return nullptr;
    }

    Ref<Texture2D> Texture2D::create(u_int32_t width, u_int32_t height)
    {
        switch(Renderer::getAPI()) {

        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLTexture2D>(width, height);
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        }

        TE_CORE_ASSERT(false, "Unknown renderer API")
        return nullptr;
    }
}