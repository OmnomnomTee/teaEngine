//
// Created by tim on 17.08.20.
//

#include "TeaEngine/Renderer/Shader.h"
#include "Platform/OpenGL/OpenGLShader.h"
#include "TeaEngine/Renderer/Renderer.h"

namespace teaEngine {
    Ref<Shader> Shader::create(const std::string& filePath)
    {
        switch(Renderer::getAPI()) {

        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLShader>(filePath);
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        }

        TE_CORE_ASSERT(false, "Unknown shader API")
        return nullptr;
    }

    Ref<Shader> Shader::create(const std::string& name, const std::string& vertexSrc, const std::string& fragmentSrc)
    {
        switch(Renderer::getAPI()) {

        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLShader>(name, vertexSrc, fragmentSrc);
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        }

        TE_CORE_ASSERT(false, "Unknown shader API")
        return nullptr;
    }

    // ---------------------- Shader Library ---------------------------
    void ShaderLibrary::add(const std::string& name, const Ref<Shader>& shader)
    {
        TE_CORE_ASSERT(m_shaders.find(name) != m_shaders.end(), "Shader name already reserved");
        m_shaders[name] = shader;
    }

    void ShaderLibrary::add(const Ref<Shader>& shader)
    {
        const std::string& shaderName = shader->getName();
        TE_CORE_ASSERT(shaderName, "No shader name set");
        TE_CORE_ASSERT(exists(name), "Shader name already reserved");
        m_shaders[shaderName] = shader;
    }

    Ref<Shader> ShaderLibrary::load(const std::string& name, const std::string& filePath)
    {
        auto shader = Shader::create(filePath);
        add(name, shader);
        return shader;
    }

    Ref<Shader> ShaderLibrary::load(const std::string& filePath)
    {
        auto shader = Shader::create(filePath);
        add(shader);
        return shader;
    }

    Ref<Shader> ShaderLibrary::load(const std::string& name, const std::string& vertexSrc, const std::string& fragmentSrc)
    {
        auto shader = Shader::create(name, vertexSrc, fragmentSrc);
        add(shader);
        return shader;
    }

    Ref<Shader> ShaderLibrary::getShader(const std::string& name)
    {
        TE_CORE_ASSERT(!exists(name), "Shader not found");
        return m_shaders[name];
    }

    bool ShaderLibrary::exists(const std::string& name)
    {
        return m_shaders.find(name) != m_shaders.end();
    }
}
