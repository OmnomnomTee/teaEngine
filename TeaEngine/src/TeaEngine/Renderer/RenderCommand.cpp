//
// Created by tim on 25.08.20.
//

#include "TeaEngine/Renderer/RenderCommand.h"
#include "Platform/OpenGL/OpenGLRendererAPI.h"

namespace teaEngine {
    RendererAPI* RenderCommand::s_rendererAPI = new OpenGLRendererAPI();
}