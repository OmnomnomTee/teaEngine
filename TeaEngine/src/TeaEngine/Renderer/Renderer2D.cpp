//
// Created by tim on 01.09.20.
//

#include "TeaEngine/Renderer/Renderer2D.h"
#include "TeaEngine/Renderer/OrthographicCamera.h"
#include "TeaEngine/Renderer/VertexArray.h"
#include "TeaEngine/Renderer/Shader.h"
#include "TeaEngine/Renderer/Renderer.h"
#include "TeaEngine/Renderer/RenderCommand.h"
#include "TeaEngine/Core/Log.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace teaEngine {
    Renderer2DStorage* Renderer2D::s_rendererData = new Renderer2DStorage();
    Ref<ShaderLibrary> Renderer2D::m_shaderLibrary = createRef<ShaderLibrary>();

    void Renderer2D::init()
    {
        s_rendererData->vertexArray = teaEngine::VertexArray::create();
        s_rendererData->vertexArray->unbind();

        // Vertex Buffer
        teaEngine::BufferLayout layout = {
                {teaEngine::ShaderDataType::Float3, "a_position" },
                {teaEngine::ShaderDataType::Float4, "a_color" },
                {teaEngine::ShaderDataType::Float2, "a_texCoords" },
                {teaEngine::ShaderDataType::Float, "a_tilingFactor" },
                {teaEngine::ShaderDataType::Float, "a_texIndex" },
        };

        s_rendererData->vertexBuffer = teaEngine::VertexBuffer::create(s_rendererData->maxVertices * sizeof(QuadVertex));
        s_rendererData->vertexBuffer->setLayout(layout);
        s_rendererData->vertexArray->addVertexBuffer(s_rendererData->vertexBuffer);
        s_rendererData->vertexBufferBase = new QuadVertex[s_rendererData->maxVertices];

        // Index Buffer
        u_int32_t* quadIndices = new u_int32_t[s_rendererData->maxIndices];

        u_int32_t offset = 0;
        for(u_int32_t i = 0; i < s_rendererData->maxIndices; i += 6) {
            quadIndices[i + 0] = offset + 0;
            quadIndices[i + 1] = offset + 1;
            quadIndices[i + 2] = offset + 2;

            quadIndices[i + 3] = offset + 2;
            quadIndices[i + 4] = offset + 3;
            quadIndices[i + 5] = offset + 0;

            offset += 4;
        }

        auto indexBuffer = teaEngine::IndexBuffer::create(quadIndices, s_rendererData->maxIndices);
        s_rendererData->vertexArray->setIndexBuffer(indexBuffer);
        delete[] quadIndices;

        // Load shaders
        s_rendererData->shaderTexture = getShaderLibrary()->load("/home/tim/Projects/Personal/teaEngine/Sandbox/assets/shaders/textureColor.glsl");
        s_rendererData->shaderTexture->bind();

        // Setup texture
        s_rendererData->textureWhite = Texture2D::create(1, 1);
        uint32_t textureWhiteData = 0xffffffff;
        s_rendererData->textureWhite->setData(&textureWhiteData, sizeof(u_int32_t));
        s_rendererData->textures[0] = s_rendererData->textureWhite;

        int samplers[teaEngine::Renderer2DStorage::maxTextureCount];
        for(int i = 0; i < teaEngine::Renderer2DStorage::maxTextureCount; i++) {
            samplers[i] = i;
        }

        s_rendererData->shaderTexture->uploadUniformIntArray("u_textures", samplers, teaEngine::Renderer2DStorage::maxTextureCount);

        s_rendererData->shaderTexture->unbind();
        // Setup renderer
        teaEngine::RenderCommand::setClearColor({0.1, 0.1, 0.1, 1});
    }

    void Renderer2D::shutdown()
    {
        delete s_rendererData;
    }

    void Renderer2D::beginScene(const OrthographicCamera& camera)
    {
        s_rendererData->shaderTexture->bind();
        s_rendererData->shaderTexture->uploadUniformMat4("u_projectionView", camera.getProjectionViewMatrix());

        s_rendererData->indexCount = 0;
        s_rendererData->vertexBufferPtr = s_rendererData->vertexBufferBase;

        s_rendererData->textureSlotIndex = 1.0f;
    }

    void Renderer2D::endScene()
    {
        u_int32_t dataSize = (u_int8_t*)s_rendererData->vertexBufferPtr - (u_int8_t*)s_rendererData->vertexBufferBase;
        s_rendererData->vertexBuffer->setData(s_rendererData->vertexBufferBase, dataSize);
        flush();
    }

    void Renderer2D::flush()
    {
        for(u_int32_t i = 0; i < s_rendererData->textureSlotIndex; i++) {
            s_rendererData->textures[i]->bind(i);
        }

        RenderCommand::drawIndexed(s_rendererData->vertexArray, s_rendererData->indexCount);

        // Stats
        s_rendererData->statistics.drawCalls++;
    }

    void Renderer2D::drawQuad(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color)
    {
        drawQuad({ position.x, position.y, 0 }, size, 0.0f, nullptr, color, 1.0f);
    }

    void Renderer2D::drawQuad(const glm::vec3& position, const glm::vec2& size, const glm::vec4& color)
    {
        drawQuad(position, size, 0.0f, nullptr, color, 1.0f);
    }

    void Renderer2D::drawQuad(const glm::vec2& position, const glm::vec2& size, float angle, const glm::vec4& color)
    {
        drawQuad({ position.x, position.y, 0 }, size, angle, nullptr, color, 1.0f);
    }

    void Renderer2D::drawQuad(const glm::vec3& position, const glm::vec2& size, float angle, const glm::vec4& color)
    {
        drawQuad({ position.x, position.y, 0 }, size, angle, nullptr, color, 1.0f);
    }

    void Renderer2D::drawQuad(const glm::vec2& position, const glm::vec2& size, const Ref<Texture2D>& texture,
            const glm::vec4& colorTint, float tilingScale)
    {
        drawQuad({ position.x, position.y, 0 }, size, 0.0f, texture, colorTint, tilingScale);
    }

    void Renderer2D::drawQuad(const glm::vec3& position, const glm::vec2& size, const Ref<Texture2D>& texture,
            const glm::vec4& colorTint, float tilingScale)
    {
        drawQuad({ position.x, position.y, 0 }, size, 0.0f, texture, colorTint, tilingScale);
    }

    void Renderer2D::drawQuad(const glm::vec2& position, const glm::vec2& size, float angle, const Ref<Texture2D>& texture, const glm::vec4& colorTint, float tilingScale)
    {
        drawQuad({ position.x, position.y, 0 }, size, angle, texture, colorTint, tilingScale);
    }

    void Renderer2D::drawQuad(const glm::vec3& position, const glm::vec2& size, float angle, const Ref<Texture2D>& texture, const glm::vec4& colorTint, float tilingScale)
    {
        if (s_rendererData->indexCount >= Renderer2DStorage::maxIndices) {
            endScene();

            s_rendererData->indexCount = 0;
            s_rendererData->vertexBufferPtr = s_rendererData->vertexBufferBase;
            s_rendererData->textureSlotIndex = 1;
        }

        float textureIndex = 0.0f;
        // Check if the texture has already a slot associated
        if(texture) {
            for(u_int32_t i = 1; i < s_rendererData->textureSlotIndex; i++) {
                if(s_rendererData->textures[i]->getRendererId() == texture->getRendererId()) {
                    textureIndex = static_cast<float>(i);
                    break;
                }
            }

            if (textureIndex == 0.0f) {
                textureIndex = static_cast<float>(s_rendererData->textureSlotIndex);
                s_rendererData->textures[s_rendererData->textureSlotIndex] = texture;
                s_rendererData->textureSlotIndex++;
            }
        }

        auto modelMatrix =
                  glm::translate(glm::mat4(1.0f), position)
                * glm::rotate(glm::mat4(1.0f), angle, { 0.0f, 0.0f, 1.0f })
                * glm::scale(glm::mat4(1.0f), { size.x, size.y, 1.0f });

        s_rendererData->vertexBufferPtr->position = modelMatrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        s_rendererData->vertexBufferPtr->color = colorTint;
        s_rendererData->vertexBufferPtr->texCoord = { 0.f, 0.f};
        s_rendererData->vertexBufferPtr->tilingFactor = tilingScale;
        s_rendererData->vertexBufferPtr->texIndex = textureIndex;
        s_rendererData->vertexBufferPtr++;

        s_rendererData->vertexBufferPtr->position = modelMatrix * glm::vec4{ size.x, 0.0f, 0.0f, 1.0f };
        s_rendererData->vertexBufferPtr->color = colorTint;
        s_rendererData->vertexBufferPtr->texCoord = { 1.f, 0.f};
        s_rendererData->vertexBufferPtr->tilingFactor = tilingScale;
        s_rendererData->vertexBufferPtr->texIndex = textureIndex;
        s_rendererData->vertexBufferPtr++;

        s_rendererData->vertexBufferPtr->position = modelMatrix * glm::vec4{  size.x, size.y, 0.0f, 1.0f};
        s_rendererData->vertexBufferPtr->color = colorTint;
        s_rendererData->vertexBufferPtr->texCoord = { 1.f, 1.f};
        s_rendererData->vertexBufferPtr->tilingFactor = tilingScale;
        s_rendererData->vertexBufferPtr->texIndex = textureIndex;
        s_rendererData->vertexBufferPtr++;

        s_rendererData->vertexBufferPtr->position = modelMatrix * glm::vec4{ 0.0f,  size.y, 0.0f, 1.0f};
        s_rendererData->vertexBufferPtr->color = colorTint;
        s_rendererData->vertexBufferPtr->texCoord = { 0.f, 1.f};
        s_rendererData->vertexBufferPtr->tilingFactor = tilingScale;
        s_rendererData->vertexBufferPtr->texIndex = textureIndex;
        s_rendererData->vertexBufferPtr++;

        s_rendererData->indexCount += 6;

        s_rendererData->statistics.quadCount++;
    }

    void Renderer2D::resetStatistics()
    {
        s_rendererData->statistics.drawCalls = 0;
        s_rendererData->statistics.quadCount = 0;
    }
}