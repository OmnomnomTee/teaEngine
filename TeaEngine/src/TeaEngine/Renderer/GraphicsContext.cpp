//
// Created by tim on 16.08.20.
//

#include "TeaEngine/Renderer/GraphicsContext.h"
#include "Platform/OpenGL/OpenGLContext.h"
#include "TeaEngine/Core/Base.h"

namespace teaEngine {

    Scope<GraphicsContext> GraphicsContext::create(void* window)
    {
        return createScope<OpenGLContext>(static_cast<GLFWwindow*>(window));
    }

}