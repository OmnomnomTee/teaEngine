//
// Created by tim on 21.08.20.
//

#include "TeaEngine/Renderer/Buffer.h"
#include "TeaEngine/Renderer/Renderer.h"
#include "TeaEngine/Renderer/RendererAPI.h"
#include "Platform/OpenGL/OpenGLBuffer.h"

namespace teaEngine {
    Ref<VertexBuffer> VertexBuffer::create(float* vertices, uint32_t size)
    {
        switch (Renderer::getAPI()) {
        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "Renderer API None is not supported");
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLVertexBuffer>(vertices, size);
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "Renderer API Vulkan is not supported");
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "Renderer API DirectX12 is not supported");
            return nullptr;
        }

        return nullptr;
    }

    Ref<VertexBuffer> VertexBuffer::create(uint32_t size)
    {
        switch (Renderer::getAPI()) {
        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "Renderer API None is not supported");
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLVertexBuffer>(size);
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "Renderer API Vulkan is not supported");
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "Renderer API DirectX12 is not supported");
            return nullptr;
        }

        return nullptr;
    }

    Ref<IndexBuffer> IndexBuffer::create(u_int32_t* indices, uint32_t count)
    {
        switch (Renderer::getAPI()) {
        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "Renderer API None is not supported");
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLIndexBuffer>(indices, count);
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "Renderer API Vulkan is not supported");
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "Renderer API DirectX12 is not supported");
            return nullptr;
        }

        return nullptr;
    }
}
