//
// Created by tim on 01.09.20.
//

#include "TeaEngine/Renderer/OrthographicCameraController.h"
#include "TeaEngine/Core/Input.h"
#include "TeaEngine/Core/KeyCodes.h"

namespace teaEngine {
    static const float DEFAULT_TRANSLATION_SPEED = 10.0f;
    static const float DEFAULT_ROTATION_SPEED = 45.f;
    static const float DEFAULT_ZOOM_LEVEL = 10.0f;
    static const float MIN_ZOOM_LEVEL = 1.f;
    static const float MAX_ZOOM_LEVEL = 5000.f;

    OrthographicCameraController::OrthographicCameraController(float aspectRatio)
        :
        m_aspectRatio(aspectRatio),
        m_zoomLevel(DEFAULT_ZOOM_LEVEL),
        m_rotation(0.0f),
        m_position{0.0f, 0.0f, 0.0f},
        m_translationSpeed(DEFAULT_TRANSLATION_SPEED),
        m_rotationSpeed(DEFAULT_ROTATION_SPEED),
        m_cameraBounds({  -m_aspectRatio * m_zoomLevel, m_aspectRatio * m_zoomLevel, -m_zoomLevel, m_zoomLevel }),
        m_camera(m_cameraBounds.left, m_cameraBounds.right, m_cameraBounds.bottom, m_cameraBounds.top)
    {

    }

    void OrthographicCameraController::onUpdate(Timestep ts)
    {
        // Movement - Keypad
        if(Input::isKeyPressed(TE_KEY_LEFT)) {
            m_position.x -= m_translationSpeed * ts.GetSeconds();
        }
        else if(Input::isKeyPressed(TE_KEY_RIGHT)) {
            m_position.x += m_translationSpeed * ts.GetSeconds();
        }
        else if(Input::isKeyPressed(TE_KEY_DOWN)) {
            m_position.y -= m_translationSpeed * ts.GetSeconds();
        }
        else if(Input::isKeyPressed(TE_KEY_UP)) {
            m_position.y += m_translationSpeed * ts.GetSeconds();
        }
            // Movement - WASD
        else if(Input::isKeyPressed(TE_KEY_A)) {
            m_position.x -= m_translationSpeed * ts.GetSeconds();
        }
        else if(Input::isKeyPressed(TE_KEY_D)) {
            m_position.x += m_translationSpeed * ts.GetSeconds();
        }
        else if(Input::isKeyPressed(TE_KEY_S)) {
            m_position.y -= m_translationSpeed * ts.GetSeconds();
        }
        else if(Input::isKeyPressed(TE_KEY_W)) {
            m_position.y += m_translationSpeed * ts.GetSeconds();
        }
            // Rotation
        else if(Input::isKeyPressed(TE_KEY_R) && !teaEngine::Input::isKeyPressed(TE_KEY_LEFT_CONTROL)) {
            m_rotation -= m_rotationSpeed * ts.GetSeconds();
        }
        else if(Input::isKeyPressed(TE_KEY_R) && teaEngine::Input::isKeyPressed(TE_KEY_LEFT_CONTROL)) {
            m_rotation += m_rotationSpeed * ts.GetSeconds();
        }

        m_camera.setPosition(m_position);
        m_camera.setRotation(m_rotation);
    }

    void OrthographicCameraController::onEvent(Event& e)
    {
        EventDispatcher dispatcher(e);
        dispatcher.dispatch<MouseScrolledEvent>(TE_BIND_EVENT_FN(OrthographicCameraController::onMouseScrolled));
        dispatcher.dispatch<WindowResizeEvent>(TE_BIND_EVENT_FN(OrthographicCameraController::onWindowResized));
    }

    bool OrthographicCameraController::onMouseScrolled(MouseScrolledEvent& event)
    {
        m_zoomLevel -= event.getYOffset() * 0.7f;
        if(m_zoomLevel < MIN_ZOOM_LEVEL) m_zoomLevel = MIN_ZOOM_LEVEL;
        if(m_zoomLevel > MAX_ZOOM_LEVEL) m_zoomLevel = MAX_ZOOM_LEVEL;
        m_cameraBounds = { -m_aspectRatio * m_zoomLevel, m_aspectRatio * m_zoomLevel, -m_zoomLevel, m_zoomLevel};
        m_camera.setProjection(m_cameraBounds.left, m_cameraBounds.right, m_cameraBounds.bottom, m_cameraBounds.top);
        return false;
    }

    bool OrthographicCameraController::onWindowResized(WindowResizeEvent& event)
    {
        m_aspectRatio = static_cast<float>(event.getWidth()) / static_cast<float>(event.getHeight());
        m_cameraBounds = { -m_aspectRatio * m_zoomLevel, m_aspectRatio * m_zoomLevel, -m_zoomLevel, m_zoomLevel};
        m_camera.setProjection(m_cameraBounds.left, m_cameraBounds.right, m_cameraBounds.bottom, m_cameraBounds.top);
        return false;
    }
}