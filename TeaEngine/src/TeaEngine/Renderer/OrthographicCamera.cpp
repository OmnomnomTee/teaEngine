//
// Created by tim on 26.08.20.
//

#include "TeaEngine/Renderer/OrthographicCamera.h"
#include <glm/gtc/matrix_transform.hpp>

namespace teaEngine {

    OrthographicCamera::OrthographicCamera(float left, float right, float bottom, float top)
        :   m_projectionMatrix{ glm::ortho(left, right, bottom, top, -1.0f, 1.0f) },
            m_viewMatrix{ 1.0f },
            m_projectionViewMatrix{ 1.0f },
            m_position{ 0.0f },
            m_rotation( 0.0f )
    {
        recalculateViewMatrix();
    }

    OrthographicCamera::OrthographicCamera(float left, float right, float bottom, float top, float zNear, float zFar)
            :   m_projectionMatrix{ glm::ortho(left, right, bottom, top, zNear, zFar) },
                m_viewMatrix{ 1.0f },
                m_projectionViewMatrix{ 1.0f },
                m_position{ 0.0f },
                m_rotation( 0.0f )
    {
        recalculateViewMatrix();
    }

    const glm::vec3& OrthographicCamera::getPosition() const
    {
        return m_position;
    }


    float OrthographicCamera::getRotation() const
    {
        return m_rotation;
    }

    void OrthographicCamera::setPosition(const glm::vec3& position)
    {
        m_position = position;
        recalculateViewMatrix();
    }

    void OrthographicCamera::setRotation(float rotation)
    {
        m_rotation = rotation;
        recalculateViewMatrix();
    }

    const glm::mat4& OrthographicCamera::getProjectionMatrix() const
    {
        return m_projectionMatrix;
    }

    const glm::mat4& OrthographicCamera::getViewMatrix() const
    {
        return m_viewMatrix;
    }

    const glm::mat4& OrthographicCamera::getProjectionViewMatrix() const
    {
        return m_projectionViewMatrix;
    }

    void OrthographicCamera::recalculateViewMatrix()
    {
        glm::mat4 transform = glm::translate(glm::mat4(1.0f), m_position) * glm::rotate(glm::mat4(1.0f), glm::radians(m_rotation), glm::vec3(0.0f, 0.0f, 1.0f));
        m_viewMatrix = glm::inverse(transform);
        m_projectionViewMatrix = m_projectionMatrix * m_viewMatrix;
    }

    void OrthographicCamera::setProjection(float left, float right, float bottom, float top)
    {
        m_projectionMatrix = glm::ortho(left, right, bottom, top, -1.0f, 1.0f);
        m_projectionViewMatrix = m_projectionMatrix * m_viewMatrix;
    }

}