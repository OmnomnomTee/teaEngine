//
// Created by tim on 23.08.20.
//

#include <Platform/OpenGL/OpenGLVertexArray.h>
#include "TeaEngine/Renderer/VertexArray.h"

#include "TeaEngine/Renderer/Renderer.h"
#include "TeaEngine/Renderer/RendererAPI.h"


namespace teaEngine {

    Ref<VertexArray> VertexArray::create()
    {
        switch(Renderer::getAPI()) {

        case RendererAPI::API::None:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::OpenGL:
            return createRef<OpenGLVertexArray>();
        case RendererAPI::API::Vulkan:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        case RendererAPI::API::DirectX12:
            TE_CORE_ASSERT(false, "RendererAPI::None is currently not supported.")
            return nullptr;
        }

        TE_CORE_ASSERT(false, "Unknown renderer API")
        return nullptr;
    }
}