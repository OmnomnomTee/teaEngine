//
// Created by tim on 11.01.19.
//

#include "TeaEngine/Core/EntryPoint.h"
#include "TeaEngine/Core/Log.h"

#include <bits/unique_ptr.h>

// Linux
#ifdef __linux__
int main(int argc, char** argv)
{
    teaEngine::Log::init();

    // Define entry point
    auto app = teaEngine::createApplication();

    // Load modules

    // Run
    app->run();

    // Clean Up
    delete app;

    return 0;
}

// WINDOWS 32 Bit && 64 Bit
#elif _WIN32
int main(int argc, char** argv)
    {
        auto app = createApplication();

        app->run();

        delete app;
    }

// Free BSD
#elif __FreeBSD__
    // Not supported
#endif
