//
// Created by tim on 15.08.20.
//

#include "TeaEngine/Core/Window.h"
#include "TeaEngine/Core/Base.h"

#include "Platform/Linux/LinuxWindow.h"

namespace teaEngine
{
    Scope<Window> Window::create(const WindowProps& props)
    {
#ifdef TE_PLATFORM_WINDOWS
            return createScope<WindowsWindow>(props);
#elif defined(TE_PLATFORM_LINUX)
        return createScope<LinuxWindow>(props);
#else
            TE_CORE_ASSERT(false, "Unknown platform");
            return nullptr;
#endif
    }
}