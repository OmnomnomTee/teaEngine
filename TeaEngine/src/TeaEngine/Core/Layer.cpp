//
// Created by tim on 16.08.20.
//

#include "TeaEngine/Core/Layer.h"

#include <utility>
#include <string>

namespace teaEngine {

    Layer::Layer(std::string  debugName)
            : m_debugName(std::move(debugName))
    {
    }

}