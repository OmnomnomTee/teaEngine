//
// Created by Tim Moehring on 23.06.18.
//

#include <TeaEngine/Events/ApplicationEvent.h>
#include "TeaEngine/Core/Application.h"
#include "TeaEngine/Core/Base.h"
#include "TeaEngine/Renderer/Renderer.h"
#include "TeaEngine/Renderer/RenderCommand.h"
#include "TeaEngine/Renderer/OrthographicCamera.h"

#include <GLFW//glfw3.h>
#include <TeaEngine/Renderer/Renderer2D.h>

namespace teaEngine {
    Application* Application::s_Instance = nullptr;

    Application::Application(const std::string& name)
    {
        s_Instance = this;

        m_window = Window::create(WindowProps(name));
        m_window->setEventCallback(TE_BIND_EVENT_FN(Application::onEvent));

        Renderer::init();

        m_imGuiLayer = new ImGuiLayer();
        pushOverlay(m_imGuiLayer);

        Renderer::init();
        Renderer2D::init();
    }

    Application::~Application() {

    }

    void Application::pushLayer(Layer* layer)
    {
        m_layerStack.pushLayer(layer);
        layer->onAttach();
    }

    void Application::pushOverlay(Layer* layer)
    {
        m_layerStack.pushOverlay(layer);
        layer->onAttach();
    }

    void Application::run()
    {
        while (m_running) {
            auto time = static_cast<float>(glfwGetTime());
            Timestep timestep(time - m_lastFrameTime);
            m_lastFrameTime = time;

            if (!m_minimized)
            {
                {
                    for (Layer* layer : m_layerStack)
                        layer->onUpdate(timestep);
                }
            }

            m_imGuiLayer->begin();
            {
                for (Layer* layer : m_layerStack)
                    layer->onImGuiRender();
            }
            m_imGuiLayer->end();

            m_window->onUpdate();
        }
    }

    void Application::onEvent(Event& e)
    {
        EventDispatcher dispatcher(e);
        dispatcher.dispatch<WindowCloseEvent>(TE_BIND_EVENT_FN(Application::onWindowClose));
        dispatcher.dispatch<WindowResizeEvent>(TE_BIND_EVENT_FN(Application::onWindowResize));

        for (auto it = m_layerStack.rbegin(); it != m_layerStack.rend(); ++it)
        {
            if (e.handled)
                break;
            (*it)->onEvent(e);
        }
    }

    bool Application::onWindowClose(WindowCloseEvent& e)
    {
        m_running = false;
        return true;
    }

    bool Application::onWindowResize(WindowResizeEvent& e)
    {

        if (e.getWidth() == 0 || e.getHeight() == 0)
        {
            m_minimized = true;
            return false;
        }

        m_minimized = false;

        Renderer::onWindowResize(e.getWidth(), e.getHeight());

        return false;
    }

    void Application::close()
    {
        m_running = false;
    }
}