# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.3.6] - 14.09.2020

### Added

#### Sandbox
- Particle system
    - Basic system to use quads as particles
    - Emission on left mouse click
    - Particle system settings
- Settings to control vsync
- Performance statistics overview

### Changed

#### Tea Engine
- Renderer 
    - Mode from immediate to deferred 
    - Use batch rendering
- Orthographic camera controller
    - Add camera bounds property
    - Use camera bounds to compute projection matrix

## [0.3.5] - 01.09.2020

### Added
- Add a shader management system
- Orthographic camera controller
- Add window resizing
- A flat color shader file

### Changed
- Add precompiled headers to CMakeLists.txt
- Change the includes to reflect the precompiled header change
- Extracted layer of the sandbox application to its own file
- Change the flat color picker from vec3 to vec4 to include alpha

## [0.3.4] - 30.08.2020

### Added
- ImGuiFileDialog
- dirent, is a dependency of ImGuiFileDialog for Windows
- stb_image for image loading
- Texture abstraction
- GUI to load images from the file dialog

## [0.3.3] - 26.08.2020

### Added
- ImGUI library
- Precompiled headers (CMake 3.16)
- Shader abstraction

### Fixed
- Cmake target_compile_features did not export the cpp version

## [0.3.2] - 16.08.2020

### Added
- Windows system
- Event system

## [0.3.1] - 26.08.2020

### Added
- Layer system

### Changed
- Update to cpp 17

### Removed
- Remove custom allocators

## [0.2.3] - 03.04.2019

### Added
- Custom memory allocators 

## [0.2.2] - 26.03.2019

### Removed
- Event system overhaul 

## [0.2.1] - 29.01.2019

### Added
-  Event system overhaul 

## [0.2.0] - 06.01.2019

### Changed
- Overhaul

## [0.1.0] - 26.06.2018

### Changed
- Project initialization
- Add OpenGL with the glad loader