//
// Created by tim on 01.09.20.
//

#include <TeaEngine.h>
#include "Sandbox2D.h"

Sandbox2D::Sandbox2D()
        :Layer("Sandbox2D Layer"),
         m_backgroundColor{1.0f},
         m_backgroundTiling(10.0f),
         m_cameraController{16.0f/9.0f},
         m_particleSystem{10000},
         m_particle{},
         activeTool(false)
{
}

void Sandbox2D::onAttach()
{
    m_textureCheckerboard = teaEngine::Texture2D::create(
            "/home/tim/Projects/Personal/teaEngine/Sandbox/assets/textures/checkerboard.png");
    m_textureStone = teaEngine::Texture2D::create(
            "/home/tim/Projects/Personal/teaEngine/Sandbox/assets/textures/obj_stone_large.png");

    m_particle.colorBegin = {254/255.f, 212/255.f, 123/255.f, 1.0f};
    m_particle.colorEnd = {254/255.f, 109/255.f, 41/255.f, 1.0f};
    m_particle.sizeBegin = 0.5f;
    m_particle.sizeEnd = 0.0f;
    m_particle.sizeVariation = 0.5f;
    m_particle.lifeTime = 5.0f;
    m_particle.velocity = {0.0f, 0.0f};
    m_particle.velocityVariation = {3.0f, 1.0f};
    m_particle.position = {0.0f, 0.0f};
}

void Sandbox2D::onDetach()
{
}

void Sandbox2D::onUpdate(teaEngine::Timestep ts)
{
    // Logic update
    m_cameraController.onUpdate(ts);

    // Rendering preparation
    teaEngine::RenderCommand::clear();

    // Rendering 2D scene
    {
        teaEngine::Renderer2D::resetStatistics();
        teaEngine::Renderer2D::beginScene(m_cameraController.getCamera());

        teaEngine::Renderer2D::drawQuad({-50.0f, -50.0f, -0.1f}, {10.0f, 10.0f}, 0.0f, m_textureCheckerboard,
                m_backgroundColor, m_backgroundTiling);
        // teaEngine::Renderer2D::drawQuad({0.0f, 0.0f, 0.1f}, {5.f, 5.f}, m_degree, nullptr, {0.0f, 0.0f, 1.0f, 1.0f}, 1.0f);

//        for(int y = 0; y < 200; y+=2) {
//            for(int x = 0; x < 200; x+=2) {
//                teaEngine::Renderer2D::drawQuad({x, y, 0.1f}, {1.f, 1.f}, 0.0f, m_textureStone, glm::vec4{1.f}, 1.0f);
//            }
//        }

        teaEngine::Renderer2D::endScene();
    }

    {
        if (teaEngine::Input::isMouseButtonPressed(TE_MOUSE_BUTTON_LEFT)) {
            auto[x, y] = teaEngine::Input::getMousePosition();
            auto width = teaEngine::Application::get().getWindow().getWidth();
            auto height = teaEngine::Application::get().getWindow().getHeight();

            auto bounds = m_cameraController.getBounds();
            auto pos = m_cameraController.getCamera().getPosition();
            x = (x/width)*bounds.getWidth()-bounds.getWidth()*0.5f;
            y = bounds.getHeight()*0.5f-(y/height)*bounds.getHeight();
            m_particle.position = {x+pos.x, y+pos.y};
            for (int i = 0; i<5; i++) {
                m_particleSystem.emit(m_particle);
            }
        }

        m_particleSystem.onUpdate(ts);
        m_particleSystem.onRender(m_cameraController.getCamera());
    }

}

void Sandbox2D::onImGuiRender()
{
    ImGui::Begin("Settings", &activeTool, ImGuiWindowFlags_MenuBar);

    if (ImGui::CollapsingHeader("Particle System")) {
        ImGui::ColorEdit4("Start Color", glm::value_ptr(m_particle.colorBegin));
        ImGui::ColorEdit4("End Color", glm::value_ptr(m_particle.colorEnd));
        ImGui::SliderFloat("Size Begin", &m_particle.sizeBegin, 0.1f, 2.f);
        ImGui::SliderFloat("Size End", &m_particle.sizeEnd, 0.1f, 2.f);
        ImGui::SliderFloat("Lifetime", &m_particle.lifeTime, 0.1f, 10.f);
    }

    if (ImGui::CollapsingHeader("Background")) {
        // open Dialog Simple
        if (ImGui::Button("Texture Map"))
            igfd::ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose Texture", ".png,.jpg,.jpeg", ".");

        // display
        if (igfd::ImGuiFileDialog::Instance()->FileDialog("ChooseFileDlgKey")) {
            // action if OK
            if (igfd::ImGuiFileDialog::Instance()->IsOk) {
                std::string filePathName = igfd::ImGuiFileDialog::Instance()->GetFilePathName();
                m_textureCheckerboard = teaEngine::Texture2D::create(filePathName);
            }
            // close
            igfd::ImGuiFileDialog::Instance()->CloseDialog("ChooseFileDlgKey");
        }

        ImGui::ColorEdit4("Color", glm::value_ptr(m_backgroundColor));
        ImGui::SliderFloat("Tiling", &m_backgroundTiling, 1.f, 5.f);
    }

    // Graphics Settings
    if (ImGui::CollapsingHeader("Settings")) {
        if (ImGui::Checkbox("VSync", &m_vSync)) {
            teaEngine::Application::get().getWindow().setVSync(m_vSync);
        }
    }

    // Statistics
    if (ImGui::CollapsingHeader("Statistics")) {
        // Application
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f/ImGui::GetIO().Framerate,
                ImGui::GetIO().Framerate);

        m_frameTimes[m_frameTimeIndex] = ImGui::GetIO().Framerate;
        ImGui::PlotLines("Frame Time  (ms/frame)", m_frameTimes, IM_ARRAYSIZE(m_frameTimes));
        m_frameTimeIndex = m_frameTimeIndex<m_frameTimesCount ? m_frameTimeIndex += 1 : 0;

        // Renderer 2D Statistics
        auto stats = teaEngine::Renderer2D::getStatistics();
        ImGui::Text("Renderer2D Statistics");
        ImGui::Text("Draw calls: %d", stats.drawCalls);
        ImGui::Text("Quad count: %d", stats.quadCount);
        ImGui::Text("Vertex count: %d", stats.getVertexCount());
        ImGui::Text("Index count: %d", stats.getIndexCount());
    }

    ImGui::End();
}

void Sandbox2D::onEvent(teaEngine::Event& event)
{
    m_cameraController.onEvent(event);
}
