//
// Created by tim on 11.01.19.
//

#include <lib/ImGuiFileDialog/include/ImGuiFileDialog.h>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Sandbox2D.h>
#include "TeaEngine.h"
#include "SandboxApplication.h"


sandbox::SandboxApplication::SandboxApplication()
    : teaEngine::Application()
{
    pushLayer(new Sandbox2D());
}

sandbox::SandboxApplication::~SandboxApplication() {

}

