//
// Created by tim on 13.09.20.
//

#include "ParticleSystem.h"

ParticleSystem::ParticleSystem(u_int32_t maxParticles)
    : m_poolIndex{maxParticles -1 }, rd(), gen(rd()), dis{0.0f, 1.0f }
{
    m_particlePool.resize(maxParticles);
}

void ParticleSystem::onUpdate(teaEngine::Timestep timestep)
{
    for(auto& particle : m_particlePool) {
        if(!particle.active) {
            continue;
        }

        if(particle.lifeRemaining <= 0.0f) {
            particle.active = false;
            continue;
        }

        particle.lifeRemaining -= timestep.GetSeconds();
        particle.position += particle.velocity * timestep.GetSeconds();
        particle.rotation += particle.rotation * timestep.GetSeconds();
    }
}

void ParticleSystem::onRender(teaEngine::OrthographicCamera orthographicCamera)
{
    teaEngine::Renderer2D::beginScene(orthographicCamera);
    for (auto& particle : m_particlePool) {
        if(!particle.active){
            continue;
        }

        // Fade out particle color
        float life = particle.lifeRemaining / particle.lifeTime;
        glm::vec4 color = glm::lerp(particle.colorEnd, particle.colorBegin, life);

        float size = glm::lerp(particle.sizeEnd, particle.sizeBegin, life);

        teaEngine::Renderer2D::drawQuad({ particle.position.x,  particle.position.y }, { size, size }, particle.rotation, color);
    }
    teaEngine::Renderer2D::endScene();
}

void ParticleSystem::emit(const ParticleProps& particleProps)
{
    Particle& particle = m_particlePool[m_poolIndex];
    particle.active = true;
    particle.position = particleProps.position;
    particle.rotation = static_cast<float>(dis(gen)) * glm::pi<float>();

    // Velocity
    particle.velocity = particleProps.velocity;
    particle.velocity.x += particleProps.velocityVariation.x *  (static_cast<float>(dis(gen))) - 0.5f;
    particle.velocity.y += particleProps.velocityVariation.y *  ( static_cast<float>(dis(gen))) - 0.5f;

    // Color
    particle.colorBegin = particleProps.colorBegin;
    particle.colorEnd = particleProps.colorEnd;

    // Lifetime
    particle.lifeTime = particleProps.lifeTime;
    particle.lifeRemaining = particleProps.lifeTime;

    // Size
    particle.sizeBegin = particleProps.sizeBegin + particleProps.sizeVariation * (static_cast<float>(dis(gen)) - 0.5f);
    particle.sizeEnd = particleProps.sizeEnd;

    m_poolIndex = --m_poolIndex % m_particlePool.size();
}
