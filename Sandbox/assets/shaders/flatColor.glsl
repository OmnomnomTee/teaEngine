// Flat Color Shader
#type vertex
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec2 a_texCoords;

uniform mat4 u_projectionView;
uniform mat4 u_modelMatrix;

void main()
{
    gl_Position = u_projectionView * u_modelMatrix * vec4(a_position, 1);
}

#type fragment
#version 330 core

layout(location = 0) out vec4 o_color;

uniform vec4 u_color;

void main()
{
    o_color = u_color;
}