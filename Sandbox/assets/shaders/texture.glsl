#type vertex
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec2 a_texCoords;

uniform mat4 u_projectionView;
uniform mat4 u_modelMatrix;

out vec2 v_texCoords;

void main()
{
    v_texCoords = a_texCoords;
    gl_Position = u_projectionView * u_modelMatrix * vec4(a_position, 1);
}

#type fragment
#version 330 core

layout(location = 0) out vec4 o_color;

in vec2 v_texCoords;

uniform sampler2D u_texture;

void main()
{
    o_color = texture(u_texture, v_texCoords);
}
