#type vertex
#version 440 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec4 a_color;
layout(location = 2) in vec2 a_texCoords;
layout(location = 3) in float a_tilingFactor;
layout(location = 4) in float a_texIndex;

uniform mat4 u_projectionView;

out vec4 v_color;
out vec2 v_texCoords;
out float v_texTiling;
out float v_texIndex;

void main()
{
    v_color = a_color;
    v_texCoords = a_texCoords;
    v_texTiling = a_tilingFactor;
    v_texIndex = a_texIndex;
    gl_Position = u_projectionView  * vec4(a_position, 1.0f);
}

#type fragment
#version 440 core

layout(location = 0) out vec4 o_color;

in vec4 v_color;
in vec2 v_texCoords;
in float v_texTiling;
in float v_texIndex;

uniform sampler2D u_textures[24];

void main()
{
    o_color = texture(u_textures[int(v_texIndex)], v_texCoords * v_texTiling) * v_color;
}
