//
// Created by tim on 01.09.20.
//

#ifndef TEAENGINECOMPLETE_SANDBOX2D_H
#define TEAENGINECOMPLETE_SANDBOX2D_H

#include <TeaEngine.h>
#include "ParticleSystem.h"

class Sandbox2D : public teaEngine::Layer {
    public:
    Sandbox2D();
    ~Sandbox2D() override = default;

    void onAttach() override;

    void onDetach() override;

    void onUpdate(teaEngine::Timestep ts) override;

    void onImGuiRender() override;

    void onEvent(teaEngine::Event& event) override;

private:
    teaEngine::Ref<teaEngine::Texture2D> m_textureCheckerboard;
    teaEngine::Ref<teaEngine::Texture2D> m_textureStone;

    glm::vec4 m_backgroundColor;
    float m_backgroundTiling;

    teaEngine::OrthographicCameraController m_cameraController;
    ParticleSystem m_particleSystem;
    ParticleProps m_particle;

    // ImGui
    bool activeTool;
    bool m_vSync = true;
    int m_frameTimesCount = 100;
    float m_frameTimes[100];
    int m_frameTimeIndex = 0;

    // Animation
    float m_degree = 0.0f;
};

#endif //TEAENGINECOMPLETE_SANDBOX2D_H
