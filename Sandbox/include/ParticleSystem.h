//
// Created by tim on 13.09.20.
//

#ifndef TEAENGINECOMPLETE_PARTICLESYSTEM_H
#define TEAENGINECOMPLETE_PARTICLESYSTEM_H

#include "TeaEngine.h"
#include <random>

struct ParticleProps {
    glm::vec2 position;
    glm::vec2 velocity;
    glm::vec2 velocityVariation;
    glm::vec4 colorBegin;
    glm::vec4 colorEnd;
    float sizeBegin;
    float sizeEnd;
    float sizeVariation;
    float lifeTime = 1.0f;
};

struct Particle {
    glm::vec2 position;
    glm::vec2 velocity;
    glm::vec4 colorBegin;
    glm::vec4 colorEnd;
    float rotation = 0.0f;
    float sizeBegin;
    float sizeEnd;
    float lifeTime = 1.0f;
    float lifeRemaining = 0.0f;

    bool active = false;
};

class ParticleSystem {
public:
    ParticleSystem(u_int32_t maxParticles);

    void onUpdate(teaEngine::Timestep timestep);
    void onRender(teaEngine::OrthographicCamera orthographicCamera);

    void emit(const ParticleProps& particleProps);
private:
    std::vector<Particle> m_particlePool;
    u_int32_t m_poolIndex;
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<> dis;
};

#endif //TEAENGINECOMPLETE_PARTICLESYSTEM_H
