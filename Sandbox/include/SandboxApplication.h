//
// Created by tim on 11.01.19.
//

#ifndef PROJECT_SANDBOXAPPLICATION_H
#define PROJECT_SANDBOXAPPLICATION_H

#include <bits/unique_ptr.h>
#include "TeaEngine.h"

namespace sandbox {

class SandboxApplication  : public teaEngine::Application{
    public:
        SandboxApplication();
        ~SandboxApplication() override;
    };
}

teaEngine::Application* teaEngine::createApplication()
{
    return new sandbox::SandboxApplication();
}

#endif //PROJECT_SANDBOXAPPLICATION_H
