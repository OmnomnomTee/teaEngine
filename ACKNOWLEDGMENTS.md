#Acknowledgments

We would like to thank all of our supporters, it means a lot to us that you help us out.

## Artists

We thank all artists whose work we can use with there permission. 

#### Lisa Fotios
[Project icon](https://www.pexels.com/photo/photography-of-blue-ceramic-coffee-cup-734983/)

## Coders

We thank all coders whose code inspired us.

#### Yan Chernikov
[TheChernoProject](https://www.youtube.com/user/TheChernoProject)

