# Tea Engine

[![pipeline status](https://gitlab.com/OmnomnomTee/teaEngine/badges/develop/pipeline.svg)](https://gitlab.com/OmnomnomTee/teaEngine/commits/develop)

The Tea Engine is a personal project to learn more about game engine development and OpenGL.

At the moment the project contains two applications. The engine itself and a sandbox application to demonstrate the 
capabilities of it.

## Getting Started

### Prerequisites
It is important that your graphics card supports **OpenGl 4.5** and upwards. You can check your OpenGL version with:
```bash
glxinfo | grep 'version'
```
Search for something similar to: OpenGL core profile version string: 4.6 (Core Profile)

The CMake files require version 3.17.3 to make use of target_precompile_headers. You can check your CMake version with:
```bash
$ cmake --version
```

##### Ubuntu 18.04
To install all needed dependencies you can type in the following commands:

```bash
$ sudo apt update
$ sudo apt install -y build-essential cmake pkg-config

Optional
$ sudo apt install -y ninja-build
```

Clone the project and pull the submodules:
```bash
# SSH
$ git clone --recurse-submodules -j8 git@gitlab.com:OmnomnomTee/teaEngine.git

# HTTPS
$ git clone --recurse-submodules -j8 https://gitlab.com/OmnomnomTee/teaEngine.git
```

### Built With

To build the project download the desired version and unzip the package. Then you can choose your generator.

#### Make Generator

To only generate the engine as a shared library use:
```bash
$ cd teaEngine

$ mkdir build

$ cd build

$ cmake ../TeaEngine/

$ make
```

To generate a sandbox application that uses the engine use:
```bash
$ cd teaEngine

$ mkdir build

$ cd build

$ cmake ../Sandbox/

$ make
```

#### Ninja Generator

To only generate the engine as a shared library use:
```bash
$ cd teaEngine

$ mkdir build

$ cd build

$ cmake -G ninja ../TeaEngine/

$ ninja
```

To generate a sandbox application that uses the engine use:
```bash
$ cd teaEngine

$ mkdir build

$ cd build

$ cmake -G ninja ../Sandbox/

$ ninja
```

If all dependencies are met you should have an executable called teaEngine in your build directory.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/OmnomnomTee/pr_projekt/tags).

## Authors

* **Tim Moehring** - *Initial work* - [OmnomnomTee](https://gitlab.com/OmnomnomTee)

See also the list of [contributors](https://gitlab.com/OmnomnomTee/awesomePong/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

We greatly appreciate the help of our supporters. View the  [Acknowledgments](ACKNOWLEDGMENTS.md) file for details.

